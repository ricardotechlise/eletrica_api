﻿const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')

// =======================
// Configuration =========
// =======================
const app = express()
const port = process.env.PORT || 3000

app.use('/public', express.static('./public'))
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use(cors())
app.use(morgan('dev'))

// Loading controllers (with routes)
// =============================================================================
app.use('/auth', require('./api/auth/routes').init())
app.use('/projects', require('./api/projects/routes').init())
app.use('/google_alert', require('./api/google_alert/routes').init())
app.use('/item', require('./api/item/routes').init())
app.use('/item_type_kit', require('./api/item_type_kit/routes').init())
app.use('/item_types', require('./api/item_types/routes').init())
app.use('/kit', require('./api/kit/routes').init())
app.use('/legislation', require('./api/legislation/routes').init())
app.use('/logs', require('./api/logs/routes').init())
app.use('/midia', require('./api/midia/routes').init())
app.use('/organs', require('./api/organs/routes').init())
app.use('/reports', require('./api/reports/routes').init())
app.use('/sendmail', require('./api/sendmail/routes').init())
app.use('/special-content', require('./api/special-content/routes').init())
app.use('/categories', require('./api/categories/routes').init())
app.use('/telephones', require('./api/telephones/routes').init())
app.use('/unities', require('./api/unities/routes').init())
app.use('/users', require('./api/users/routes').init())
app.use('/upload', require('./api/upload/routes').init())

// =======================
// Start the server ======
// =======================
app.listen(port)
console.log('Serviço iniciado na porta %s', port)
