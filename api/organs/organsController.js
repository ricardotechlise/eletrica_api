const organs = require('./organs')

function getOrgans(req, res) {
  organs.getOrgans(req.query, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.json(data)
  })
}

function createOrgan(req, res) {
  organs.createOrgan(req.body, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.status(201).json(data)
  })
}

function updateOrgan(req, res) {
  organs.updateOrgan(req.params.id, req.body, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.status(204).json(data)
  })
}

function changeOrgan(req, res) {
  organs.changeOrgan(req.params.id, req.body, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.status(204).json(data)
  })
}

module.exports = {
  getOrgans,
  createOrgan,
  updateOrgan,
  changeOrgan
}
