const express = require('express')
const organsController = require('./organsController')
const verifyToken = require('../middlewares/verify')

function init() {
  const routes = express.Router()

  routes.route('/')
    .get(verifyToken, organsController.getOrgans)
    .post(verifyToken, organsController.createOrgan)

  routes.route('/:id')
    .put(verifyToken, organsController.updateOrgan)
    .patch(verifyToken, organsController.changeOrgan)

  return routes
}

module.exports = { init }
