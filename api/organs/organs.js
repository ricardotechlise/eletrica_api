const mysql = require('mysql')
const config = require('../../config')

const pool = mysql.createPool(config.mysqlConfig)

function getOrgans(params, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({error, message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500})

    let sql = `
                SELECT * FROM org_orgao
              `
    let values = []

    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) return callback({error, status: 400})
      return callback(null, results)
    })
  })
}

function createOrgan(data, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({error, message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500})
    
    const sql = `
                  INSERT INTO org_orgao SET ?
                `
    const values = {
      org_nome: data.org_nome
    }

    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) return callback({error, status: 400})
      return callback(null, {id: results.insertId})
    })
  })
}

function updateOrgan(id, data, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({error, message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500})
    
    const sql = `
                  UPDATE org_orgao SET ? WHERE org_orgao_id = ?
                `
    const values = [{
      org_nome: data.org_nome
    }, id]

    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) return callback({error, status: 400})
      return callback(null, null)
    })
  })
}

function changeOrgan(id, data, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({error, message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500})

    const sql = `
                  UPDATE org_orgao SET ? WHERE org_orgao_id = ?
                `
    const values = [{
      org_status: data.org_status
    }, id]

    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) return callback({error, status: 400})
      return callback(null, null)
    })
  })
}

module.exports = {
  getOrgans,
  createOrgan,
  updateOrgan,
  changeOrgan
}
