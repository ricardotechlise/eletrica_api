const express = require("express")
const usersController = require("./usersController")
const verifyToken = require('../middlewares/verify')

function init() {
	let routes = express.Router()

	routes.route('/')
    	.get(verifyToken, usersController.get)
		.post(verifyToken, usersController.post)

	routes.route('/unity/:id/')
		.get(verifyToken, usersController.getUnityUser)
        
	routes.route('/:id/')
		.get(verifyToken, usersController.getId)
		.put(verifyToken, usersController.putId)
		.patch(verifyToken, usersController.patchId)
		.delete(verifyToken, usersController.deleteId)

	return routes
}

module.exports = {init}