const users = require('./users')

function get (req, res, next) {
  users.get(req.query, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getId (req, res, next) {
  users.getId(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getUnityUser (req, res, next) {
  users.getUnityUser(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function post (req, res, next) {
  users.post (req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function putId (req, res, next) {
  users.putId(req.params, req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function patchId (req, res, next) {
  users.patchId(req.params, req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function deleteId(req, res, next){
  users.deleteId(req.params, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

module.exports = {
  get,
  getId,
  getUnityUser,
  post,
  putId,
  patchId,
  deleteId
}
