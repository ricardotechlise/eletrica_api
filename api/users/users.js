const mysql = require('mysql')
const bcrypt = require('bcryptjs')
const config = require('../../config')

var pool = mysql.createPool(config.mysqlConfig)

function get (params, callback) {
	// Listar todos os usuários
  let query = 'SELECT usr.usr_user_id, usr.usr_apelido, usr.usr_tipo, usr.usr_status, usr.usr_created_by, usr.usr_created_at, pes.pes_pessoa_id, pes.pes_uni_unidade_id, pes.pes_usr_user_id_acesso, pes.pes_nome, pes.pes_cpf, pes.pes_email, pes.pes_genero, pes.pes_created_by, pes.pes_created_at FROM usr_user usr LEFT JOIN pes_pessoa pes ON usr.usr_user_id = pes.pes_usr_user_id_acesso'
	let values = []

	if (params.pes_uni_unidade_id) {
		query += ' WHERE pes.pes_uni_unidade_id = ?'
		values = [...values, params.pes_uni_unidade_id]
	}
	
	pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release()
			if (error) return callback(error)
			return callback(null, results)
		})
	})
}

function getId (param, callback) {
	// Buscar informações do usuário
	let query = 'SELECT usr.usr_user_id, usr.usr_apelido, usr.usr_tipo, usr.usr_status, usr.usr_created_by, usr.usr_created_at, pes.pes_pessoa_id, pes.pes_uni_unidade_id, pes.pes_usr_user_id_acesso, pes.pes_nome, pes.pes_cpf, pes.pes_email, pes.pes_genero, pes.pes_created_by, pes.pes_created_at FROM usr_user usr LEFT JOIN pes_pessoa pes ON usr.usr_user_id = pes.pes_usr_user_id_acesso WHERE ?'
	let values = [{usr_user_id: param.id}]

	pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release()
			if (error) return callback(error)
			return callback(null, results)
		})
	})
}

function getUnityUser (param, callback) {
	// Buscar usuários por unidade
	let query = 'SELECT usr.usr_user_id, usr.usr_apelido, usr.usr_tipo, usr.usr_status, usr.usr_created_by, usr.usr_created_at, pes.pes_pessoa_id, pes.pes_uni_unidade_id, pes.pes_usr_user_id_acesso, pes.pes_nome, pes.pes_cpf, pes.pes_email, pes.pes_genero, pes.pes_created_by, pes.pes_created_at FROM usr_user usr LEFT JOIN pes_pessoa pes ON usr.usr_user_id = pes.pes_usr_user_id_acesso WHERE ?'
	let values = [{pes_uni_unidade_id: param.id}]

	pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release()
			if (error) return callback(error)
			return callback(null, results)
		})
	})
}

function post(data, callback) {

	let values = [{pes_cpf: data.pes_cpf},{pes_email: data.pes_email}]
	let query = 'SELECT pes_pessoa_id FROM pes_pessoa WHERE ? OR ?'

  pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			if (error) {
				connection.release()  
				return callback(error)
			} else {
				if (results.length) {
					return callback(null, {status: false, error: 'cpf já cadastrado'})
				} else {
					let pes_usr_user_id_acesso = null

					query = 'INSERT INTO usr_user SET ?'
					values = [
						{
							usr_apelido: data.usr_apelido, 
							usr_senha: bcrypt.hashSync(data.usr_senha, 8), 
							usr_tipo: data.usr_tipo, 
							usr_created_by: data.usr_created_by
						}
					]
					pool.getConnection((err, connection) => {
						connection.query(query, values, (error, results, fields) => {
							if (error) {
								connection.release()  
								return callback(error)
							} else {
								query = "SELECT LAST_INSERT_ID()"
								connection.query(query, values, (error, results, fields) => {
									if (error) {
										connection.release()  
										return callback(error)
									} else {
										pes_usr_user_id_acesso = results[0]['LAST_INSERT_ID()']
										query = 'INSERT INTO pes_pessoa SET ?'
											values = [
												{
													pes_uni_unidade_id: data.pes_uni_unidade_id, 
													pes_usr_user_id_acesso: pes_usr_user_id_acesso, 
													pes_nome: data.pes_nome, 
													pes_cpf: data.pes_cpf, 
													pes_email: data.pes_email, 
													pes_genero: data.pes_genero, 
													pes_created_by: data.pes_created_by
												}
											]
											connection.query(query, values, (error, results, fields) => {
												connection.release() 
												if (error) return callback(error)
												return callback(null, {success: true})
											})
									}
								})
							}
						})
					})
				}
			}
		})
	})
}

function putId(param, data, callback) {
  // Atualizar informações do usuário por ID
  let query = 'UPDATE usr_user usr LEFT JOIN pes_pessoa pes ON usr.usr_user_id = pes.pes_usr_user_id_acesso SET ? WHERE ?'
  let values = [
		{
			usr_apelido: data.usr_apelido,
			usr_tipo: data.usr_tipo,
			pes_uni_unidade_id: data.pes_uni_unidade_id,
			pes_nome: data.pes_nome,
			pes_cpf: data.pes_cpf,
			pes_email: data.pes_email,
			pes_genero: data.pes_genero
		},
		{
			usr_user_id: param.id
		}
	]

	pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, {success: true})
    })
  })
}

function patchId(param, data, callback) {
  // Atualizar informações do usuário por ID
  let query = 'UPDATE usr_user usr LEFT JOIN pes_pessoa pes ON usr.usr_user_id = pes.pes_usr_user_id_acesso SET ? WHERE ?'
	
	for (var key in data) {
		var value = data[key]
		if (key == 'usr_senha') {
			data[key] = bcrypt.hashSync(data[key], 8)
		}
	}
	
	let values = [data, {usr_user_id: param.id}]
	
	pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, {success: true})
    })
  })
}

function deleteId(param, callback) {
  // Excluir o usuário
	let query = 'DELETE FROM usr_user WHERE ?'
	let values = [{usr_user_id: param.id}]
  
  pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
    	connection.release()
      if (error) return callback(error)
      return callback(null, {success: true})
    })
  })
}

module.exports = {
	get,
	getId,
	getUnityUser,
	post,
	putId,
	patchId,
	deleteId
}