const mysql = require('mysql')
const nodemailer = require('nodemailer')
const config = require('../../config')

var pool = mysql.createPool(config.mysqlConfig)

function post(data, callback) {

  if (data.ite_item_id) {

    let query = ''
    let values = []

    console.log(data)

    query = ` SELECT 	uni.uni_email, `
      + ` 			uni.uni_razao_social, `
      + ` 			emp.emp_nome, `
      + ` 			tii.tii_descricao, `
      + ` 			tiu.tiu_quant, `
      + ` 			tiu.tiu_quant_min `
      + ` FROM ite_item ite `
      + ` JOIN tii_tipo_item tii ON ite.ite_tii_tipo_item_id = tii.tii_tipo_item_id `
      + ` JOIN uni_unidade uni ON ite.ite_uni_unidade_id = uni.uni_unidade_id `
      + ` JOIN emp_empresa emp ON uni.uni_emp_empresa_id = emp.emp_empresa_id `
      + ` JOIN tiu_tipo_item_unidade tiu ON ite.ite_uni_unidade_id = tiu.tiu_uni_unidade_id AND ite.ite_tii_tipo_item_id = tiu.tiu_tii_tipo_item_id `
      + ` WHERE ite.ite_item_id = ? `

    values = [data.ite_item_id]

    pool.getConnection((err, connection) => {
      connection.query(query, values, (error, results, fields) => {
        if (error) {
          connection.release()
          return callback(error)
        } else {

          console.log(results)

          data = results[0]

          console.log(data)

          if (data &&
            data.uni_email &&
            data.uni_razao_social &&
            data.emp_nome &&
            data.tii_descricao &&
            data.tiu_quant &&
            data.tiu_quant_min) {

            let email = data.uni_email
            let unidade = data.uni_razao_social
            let empresa = data.emp_nome
            let produto = data.tii_descricao
            let quant = data.tiu_quant
            let quant_min = data.tiu_quant_min

            const transporter = nodemailer.createTransport(config.transporter)
            const mailOptions = {
              from: config.transporter.auth.user,
              to: email,
              subject: 'Alerta de estoque',
              html:
                '<body style="color: #039F4">'
                + '<h1 style="font-weight: bold;">Alerta de estoque - Produto: ' + produto + ' - ECORESPONSE.</h1>'
                + '<hr>'
                //+ '<strong>Nome: </strong>'
                + '<h3> A unidade ' + unidade + ' da empresa ' + empresa
                + ' esta com o produto ' + produto + ' com a quantidade ' + quant
                + ' e a quantidade mínima configurada é ' + quant_min + '</h3>,'
                + '<br>'
                + '</body>'
            }

            transporter.sendMail(mailOptions, (error, info) => {
              if (error) console.log(error)
              //console.log('Email sent:', info)
            })
            connection.release()
            return callback(null, { success: true })
          } else {
            connection.release()
            return callback(null, { success: 'Dados não encontrados.' })
          }
        }
      })
    })
  }
}

function createEmail(data, callback) {
  if (data.visitor) {
    const transporter = nodemailer.createTransport(config.transporter)
    let mailOptions = {
      from: config.transporter.auth.user,
      to: 'lucas.varela@techlise.com.br',
      subject: 'Solicitação de novo pedido',
      html: `
              <p>
                Nova solicitação de pedido de visitante<br>
                Informações:<br>
                Nome: ${data.nome}<br>
                Email: ${data.email}<br>
                Telefone: ${data.telefone}<br>
              </p><br>
            `
    }

    if (data.kits && data.kits.length) {
      mailOptions.html += `<h4>Kits:</h4>`
      mailOptions.html += `<ul>`

      data.kits
      .forEach(kit =>
        mailOptions.html += `<li>${kit.tii_descricao} -> ${kit.quant}</li>`)

      mailOptions.html += `</ul>`
    }

    if (data.products && data.products.length) {
      mailOptions.html += `<h4>Produtos:</h4>`
      mailOptions.html += `<ul>`

      data.products
      .forEach(product =>
        mailOptions.html += `<li>${product.tii_descricao} -> ${product.quant}</li>`)

      mailOptions.html += `</ul>`
    }    

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) return callback({ error, status: 500 })
      return callback(null, null)
    })
  } else
    pool.getConnection((error, conn) => {
      if (error)
        return callback({error, message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500})

      const sql = `
                    SELECT u.uni_nome_fantasia, e.emp_nome
                    FROM uni_unidade u
                    JOIN emp_empresa e
                      ON u.uni_emp_empresa_id = e.emp_empresa_id
                    WHERE u.uni_unidade_id = ?
                  `

      conn.query(sql, data.unity, (error, results, fields) => {
        conn.release()
        if (error)
          return callback({error, status: 400})

        if (results.length < 1)
          return callback({message: 'Id da unidade inválido.', status: 400})

        const unity = results[0]

        const transporter = nodemailer.createTransport(config.transporter)
        let mailOptions = {
          from: config.transporter.auth.user,
          to: 'lucas.varela@techlise.com.br',
          subject: 'Solicitação de novo pedido',
          html: `
                  <p>
                    Nova solicitação de pedido da empresa ${unity.emp_nome} 
                      - unidade ${unity.uni_nome_fantasia}
                  </p><br>
                `
        }

        if (data.kits && data.kits.length) {
          mailOptions.html += `<h4>Kits:</h4>`
          mailOptions.html += `<ul>`
          
          data.kits
          .forEach(kit => 
            mailOptions.html += `<li>${kit.tii_descricao} -> ${kit.quant}</li>`)

          mailOptions.html += `</ul>`
        }

        if (data.products && data.products.length) {
          mailOptions.html += `<h4>Produtos:</h4>`
          mailOptions.html += `<ul>`

          data.products
          .forEach(product => 
            mailOptions.html += `<li>${product.tii_descricao} -> ${product.quant}</li>`)

          mailOptions.html += `</ul>`
        }

        transporter.sendMail(mailOptions, (error, info) => {
          if (error) return callback({error, status: 500})
          return callback(null, null)
        })
      })
    })
}

module.exports = {
  post,
  createEmail
}
