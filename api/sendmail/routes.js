const express = require("express")
const sendmailController = require("./sendmailController")
const verifyToken = require('../middlewares/verify')

function init() {
	let routes = express.Router()

	routes.route('/')
		.post(verifyToken, sendmailController.post)

	routes.route('/manual')
		.post(sendmailController.createEmail)

	return routes
}

module.exports = {init}