const sendmail = require('./sendmail')

function post (req, res, next) {
  sendmail.post (req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function createEmail(req, res) {
  if (!req.headers['authorization'])
    req.body.visitor = true

  sendmail.createEmail(req.body, (err, data) => {
    if (err) return res.status(err.status).json(err)
    return res.status(204).json(data)
  })
}

module.exports = {
  post,
  createEmail
}
