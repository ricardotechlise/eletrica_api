const midia = require('./midia')

function get(req, res) {
  midia.get(req.query, (err, data) => {
    if (err) return res.status(err.status).json(err)
    return res.json(data)
  })
}

function post(req, res) {
  req.body.files = req.files ? req.files : {}

  midia.post(req.body, (err, data) => {
    if (err) return res.status(err.status).json(err)
    return res.status(201).json(data)
  })
}

function patchId(req, res) {
  req.body.files = req.files

  midia.patchId(req.params.id, req.body, (err, data) => {
    if (err) return res.status(500).json(err)
    return res.status(204).json(data)
  })
}

function deleteId(req, res) {
  midia.deleteId(req.params.id, (err, data) => {
    if (err) return res.status(500).json(err)
    return res.status(204).json(data)
  })
}

module.exports = {
  get,
  post,
  patchId,
  deleteId
}
