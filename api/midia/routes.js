const express = require('express')
const midiaController = require('./midiaController')
const verifyToken = require('../middlewares/verify')
const upload = require('../middlewares/upload')

function init() {
  const routes = express.Router()
  
  routes.route('/')
    .get(verifyToken, midiaController.get)
    .post(verifyToken, upload.fields([{
      name: 'image',
      maxCount: 1
    }, {
      name: 'gallery',
      maxCount: 1
    }, {
      name: 'documentation',
      maxCount: 1
    }]), midiaController.post)

  routes.route('/:id')
    .patch(verifyToken, upload.fields([{
      name: 'image',
      maxCount: 1
    }, {
      name: 'gallery',
      maxCount: 1
    }, {
      name: 'documentation',
      maxCount: 1
    }]), midiaController.patchId)
    .delete(verifyToken, midiaController.deleteId)

  return routes
}

module.exports = { init }
