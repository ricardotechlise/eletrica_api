const mysql = require('mysql')
const fs = require('fs')
const config = require('../../config')

const pool = mysql.createPool(config.mysqlConfig)

function get(params, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({error, message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500})

    let sql = `
                SELECT mid_midia_id, mid_href, mid_tipo
                FROM mid_midia
              `
    let values = []

    if (params.mid_tii_tipo_item_id) {
      sql += `
              WHERE mid_tii_tipo_item_id = ?
            `
      values = [...values, params.mid_tii_tipo_item_id]
    }

    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) return callback({error, status: 400})
      return callback(null, results)
    })
  })
}

function post(data, callback) {
  if (!data.tii_tipo_item_id && JSON.stringify(data.files) !== '{}') {
    if (data.files['image'])
      fs.unlink('./' + data.files['image'][0].path.replace(/\\/g, '/'))

    if (data.files['gallery'])
      fs.unlink('./' + data.files['gallery'][0].path.replace(/\\/g, '/'))

    if (data.files['documentation'])
      fs.unlink('./' + data.files['documentation'][0].path.replace(/\\/g, '/'))

    return callback({message: 'Não foi especificado o tipo de item das imagens.', status: 400})
  } else if (!data.tii_tipo_item_id)
    return callback({message: 'Não foi especificado o tipo de item das imagens.', status: 400})

  pool.getConnection((error, conn) => {
    if (error) {
      conn.release()
      return callback({message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500})
    }

    let sql = `
      SELECT mid.mid_midia_id, mid.mid_tipo, mid.mid_href, tii.tii_mid_midia_id_perfil 
        FROM mid_midia mid
      JOIN tii_tipo_item tii 
        ON tii_tipo_item_id = mid.mid_tii_tipo_item_id
      WHERE tii.tii_tipo_item_id = ?
    `

    conn.query(sql, data.tii_tipo_item_id, (error, results, fields) => {
      if (error) {
        conn.release()
        return callback({error, status: 422})
      }

      const hasProfile = results[0] ? results[0].tii_mid_midia_id_perfil !== null : false
      const photosInGallery = results.filter(midia => midia.mid_tipo === 'FOTO' && midia.mid_midia_id !== midia.tii_mid_midia_id_perfil).length
    
      const deleteUploads = () => {
        if (data.files['image']) fs.unlink('./' + data.files['image'][0].path.replace(/\\/g, '/'))
        if (data.files['gallery']) fs.unlink('./' + data.files['gallery'][0].path.replace(/\\/g, '/'))
        if (data.files['documentation']) fs.unlink('./' + data.files['documentation'][0].path.replace(/\\/g, '/'))
      }

      if (data.files && data.files['image'] && data.files['gallery']) {
        let errors = []

        if (hasProfile) 
          errors = errors.concat('perfil')

        if (photosInGallery > 4) 
          errors = errors.concat('galeria')

        if (errors.length > 0) {
          deleteUploads()
          conn.release()
          let message = 'Já há arquivos salvos nesse tipo de item:'
          errors.forEach((error, index) => {
            if (index !== 0) message += '/' + error
            else message += error
          })
          return callback({message, status: 422})
        }

        conn.beginTransaction(error => {
          if (error) {
            deleteUploads()
            conn.rollback()
            return callback({error, status: 500})
          }

          const profilePath = data.files['image'][0].path.replace(/\\/g, '/')
          const galleryPath = data.files['gallery'][0].path.replace(/\\/g, '/')

          sql = `INSERT INTO mid_midia SET ?`
          params = {
            mid_tii_tipo_item_id: data.tii_tipo_item_id,
            mid_href: profilePath,
            mid_tipo: 'FOTO',
            mid_created_by: data.mid_created_by
          }
          conn.query(sql, params, (error, results, fields) => {
            if (error) {
              deleteUploads()
              conn.rollback()
              return callback({error, status: 400})
            }

            const tii_mid_midia_id_perfil = results.insertId

            params.mid_href = galleryPath
            conn.query(sql, params, (error, results, fields) => {
              if (error) {
                deleteUploads()
                conn.rollback()
                return callback({error, status: 400})
              }

              const mid_midia_id = results.insertId

              sql = `UPDATE tii_tipo_item SET tii_mid_midia_id_perfil = ? WHERE tii_tipo_item_id = ?`
              params = [tii_mid_midia_id_perfil, data.tii_tipo_item_id]
              conn.query(sql, params, (error, results, fields) => {
                if (error) {
                  deleteUploads()
                  conn.rollback()
                  return callback({error, status: 400})
                }

                conn.commit(error => {
                  if (error) {
                    deleteUploads()
                    conn.rollback()
                    return callback({error, status: 500})
                  }
                  return callback(null, {mid_midia_id: tii_mid_midia_id_perfil, tii_mid_midia_id_perfil}, {mid_midia_id, tii_mid_midia_id_perfil})
                })
              })
            })
          })
        })
      } else if (data.files && data.files['image']) {
        if (hasProfile) {
          deleteUploads()
          conn.release()
          return callback({message: 'Já há arquivos salvos nesse tipo de item: perfil', status: 422})
        }

        conn.beginTransaction(error => {
          if (error) {
            deleteUploads()
            conn.rollback()
            return callback({error, status: 500})
          }

          let sql = `INSERT INTO mid_midia SET ?`
          let params = {
            mid_tii_tipo_item_id: data.tii_tipo_item_id,
            mid_href: data.files['image'][0].path.replace(/\\/g, '/'),
            mid_tipo: 'FOTO',
            mid_created_by: data.mid_created_by
          }
          conn.query(sql, params, (error, results, fields) => {
            if (error) {
              deleteUploads()
              conn.rollback()
              return callback({error, status: 400})
            }

            const tii_mid_midia_id_perfil = results.insertId

            sql = `UPDATE tii_tipo_item SET tii_mid_midia_id_perfil = ? WHERE tii_tipo_item_id = ?`
            params = [tii_mid_midia_id_perfil, data.tii_tipo_item_id]
            conn.query(sql, params, (error, results, fields) => {
              if (error) {
                deleteUploads()
                conn.rollback()
                return callback({error, status: 400})
              }

              conn.commit(error => {
                if (error) {
                  deleteUploads()
                  conn.rollback()
                  return callback({error, status: 400})
                }
                return callback(null, {mid_midia_id: tii_mid_midia_id_perfil, tii_mid_midia_id_perfil})
              })
            })
          })
        })
      } else if (data.files && data.files['gallery']) {
        if (photosInGallery > 4) {
          deleteUploads()
          conn.release()
          return callback({message: 'Já há arquivos salvos nesse tipo de item: galeria', status: 422})
        }

        const sql = `INSERT INTO mid_midia SET ?`
        const params = {
          mid_tii_tipo_item_id: data.tii_tipo_item_id,
          mid_href: data.files['gallery'][0].path.replace(/\\/g, '/'),
          mid_tipo: 'FOTO',
          mid_created_by: data.mid_created_by
        }
        conn.query(sql, params, (error, results, fields) => {
          conn.release()
          if (error) {
            deleteUploads()
            return callback({error, status: 400})
          }

          return callback(null, {mid_midia_id: results.insertId})
        })
      } else if ((data.files && data.files['documentation']) || data.documentation) {
        sql = `INSERT INTO mid_midia SET ?`

        let mid_href, mid_tipo

        if (data.files && data.files['documentation']) {
          mid_href = data.files['documentation'][0].path.replace(/\\/g, '/')
          mid_tipo = 'ARQUIVO'
        } else {
          mid_href = data.documentation.mid_href,
          mid_tipo = 'VIDEO'
        }

        params = {
          mid_tii_tipo_item_id: data.tii_tipo_item_id,
          mid_href,
          mid_tipo,
          mid_created_by: data.mid_created_by
        }

        conn.query(sql, params, (error, results, fields) => {
          if (error) {
            conn.release()
            deleteUploads()
            return callback({error, status: 400})
          }
          return callback(null, {mid_midia_id: results.insertId, mid_href, mid_tipo})
        })
      } else return callback({status: 400, message: 'Não foi passado nenhum arquivo, ou objeto válido.'})
    })
  })
}

function patchId(id, data, callback) {
  if (JSON.stringify(data.files) === '{}')
    return callback({message: 'Nenhuma imagem foi enviada.'})

  pool.getConnection((error, conn) => {
    if (error) return callback(error)

    conn.beginTransaction(err => {
      if (err) {
        conn.rollback()
        return callback(err)
      }

      let sql = `
        SELECT mid.mid_tipo, mid.mid_href, tii_mid_midia_id_perfil FROM mid_midia mid
        JOIN tii_tipo_item tii ON tii_tipo_item_id = mid.mid_tii_tipo_item_id
        WHERE mid.mid_midia_id = ?
      `

      conn.query(sql, id, (error, results, fields) => {
        if (error) {
          conn.rollback()
          return callback(error)
        }

        if (results.length < 1)
          return callback({ message: 'Sem midia existente.' })

        const midia = results[0]

        const deleteWrongUploads = new Promise((resolve, reject) => {
          if (midia.tii_mid_midia_id_perfil != id && data.files['image'] && data.files['gallery']) 
            fs.unlink('./' + data.files['image'][0].path.replace(/\\/g, '/'), error => {
              if (error) reject(error)
              resolve('gallery')
            })
          else if (midia.tii_mid_midia_id_perfil == id && data.files['gallery'] && data.files['image']) 
            fs.unlink('./' + data.files['gallery'][0].path.replace(/\\/g, '/'), error => {
              if (error) reject(error)
              resolve('image')
            })
          else if (midia.tii_mid_midia_id_perfil == id && data.files['image']) resolve('image')
          else if (midia.tii_mid_midia_id_perfil != id && data.files['gallery']) resolve('gallery')
          else reject({message: 'Id incoerente com campo mandado.'})
        })

        deleteWrongUploads
        .then(fileKey => {
          fs.unlink('./' + midia.mid_href, (err) => {
            if (err && err.code !== 'ENOENT') {
              conn.rollback()
              return callback(err)
            }

            const file = data.files[fileKey][0]
            file.path = file.path.replace(/\\/g, '/')

            sql = 'UPDATE mid_midia SET ? WHERE mid_midia_id = ?'
            const params = [{
              mid_href: file.path,
              mid_tipo: midia.mid_tipo
            }, id]

            conn.query(sql, params, (error, results, fields) => {
              if (error) {
                fs.unlink('./' + file.path)
                conn.rollback()
                return callback(error)
              }

              conn.commit(err => {
                if (err) {
                  fs.unlink('./' + file.path)
                  conn.rollback()
                  return callback(err)
                }
                return callback(null, null)
              })
            })
          })
        })
        .catch(error => {
          conn.rollback()
          return callback(error)
        })
      })
    })
  })
}

function deleteId(id, callback) {
  pool.getConnection((error, conn) => {
    if (error) return callback(error)

    conn.beginTransaction(err => {
      if (err) {
        conn.rollback()
        return callback(err)
      }

      let sql = 'SELECT mid_href FROM mid_midia WHERE mid_midia_id = ?'

      conn.query(sql, id, (error, results, fields) => {
        if (error) {
          conn.rollback()
          return callback(error)
        }

        if (results.length < 1)
          return callback({ message: 'Sem midia existente.' })

        const midia = results[0]

        fs.unlink('./' + midia.mid_href, error => {
          if (error && error.code !== 'ENOENT') {
            conn.rollback()
            return callback(error)
          }

          sql = 
            midia.mid_tipo === 'FOTO' 
              ? `UPDATE mid_midia SET mid_href = '' WHERE mid_midia_id = ?`
              : `DELETE FROM mid_midia WHERE mid_midia_id = ?`

          conn.query(sql, id, (error, results, fields) => {
            if (error) {
              conn.rollback()
              return callback(error)
            }

            conn.commit(error => {
              if (error) {
                conn.rollback()
                return callback(error)
              }
              return callback(null, null)
            })
          })
        })
      })
    })
  })
}

module.exports = {
  get,
  post,
  patchId,
  deleteId
}
