const legislation = require('./legislation')

function get(req, res, next) {
  legislation.get((err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getId(req, res, next) {
  legislation.getId(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getByCategory(req, res, next) {
  legislation.getByCategory(req.query, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function post(req, res, next) {
  legislation.post(req, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function putId (req, res, next) {
  legislation.putId(req.params.id, req, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

module.exports = {
  get,
  getId,
  getByCategory,
  post,
  putId
}
