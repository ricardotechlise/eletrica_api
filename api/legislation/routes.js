const express = require("express")
const multiparty = require('connect-multiparty')
const legislationController = require("./legislationController")
const verifyToken = require('../middlewares/verify')

function init() {
	const routes = express.Router()

	routes.route('/')
		.get(legislationController.get)
    .post(verifyToken, multiparty(), legislationController.post)
  
  routes.route('/category')
    .get(legislationController.getByCategory)

	routes.route('/:id')
		.get(legislationController.getId)
		.put(verifyToken, multiparty(), legislationController.putId)

  return routes

}

module.exports = {
	init
}