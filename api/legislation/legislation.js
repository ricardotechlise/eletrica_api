const mysql = require('mysql')
const config = require('../../config')
const mv = require('mv')
var pool = mysql.createPool(config.mysqlConfig)

function get(callback) {
    // Listar todas as noticias
    let query = 'SELECT * FROM lea_legislacao_ambiental'

    pool.getConnection((err, connection) => {
        connection.query(query, (error, results, fields) => {
            connection.release()
            if (error) return callback(error)
            return callback(null, results)
        })
    })
}

function getId(params, callback) {
    // Buscar notícias de um usuario
    let query = 'SELECT * FROM lea_legislacao_ambiental WHERE ?'
    let values = [{ lea_legislacao_ambiental_id: params.id }]
    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) return callback(error)
            return callback(null, results)
        })
    })
}

function getByCategory (param, callback) {
    var query
    var values
    if (param.est) {
        query = `SELECT * FROM lea_legislacao_ambiental WHERE ? AND ?`
        values = [{ lea_categoria: param.cat }, { lea_estado: param.est }]
    } else {
        query = `SELECT * FROM lea_legislacao_ambiental WHERE ?`
        values = [{ lea_categoria: param.cat}]
    }
    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) return callback(error)
            return callback(null, results)
        })
    })
}

function post(data, callback) {
    let query = 'INSERT INTO lea_legislacao_ambiental SET ?'
    let values = { 
        lea_titulo: data.body.lea_titulo, 
        lea_texto: data.body.lea_texto, 
        lea_estado: data.body.lea_estado, 
        lea_municipio: data.body.lea_municipio,
        lea_categoria: data.body.lea_categoria,
        lea_data: data.body.lea_data, 
        lea_org_orgao_id: data.body.lea_org_orgao_id, 
        lea_created_by: data.body.lea_created_by 
    }
    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) return callback(error)
            return callback(null, { success: true })
        })
    })
}

function putId(id, data, callback) {
    let query = 'UPDATE lea_legislacao_ambiental SET ? WHERE ?'
    let values = [{
        lea_titulo: data.body.lea_titulo,
        lea_texto: data.body.lea_texto,
        lea_estado: data.body.lea_estado,
        lea_municipio: data.body.lea_municipio,
        lea_categoria: data.body.lea_categoria,
        lea_data: data.body.lea_data,
        lea_org_orgao_id: data.body.lea_org_orgao_id
    }, { lea_legislacao_ambiental_id: id }]
    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) return callback(error)
            return callback(null, { success: true })
        })
    })
}

module.exports = {
    get,
    getId,
    getByCategory,
    post,
    putId
}