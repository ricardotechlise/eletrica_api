const upload = require('./upload.js')

function get (req, res, next) {
  upload.get((err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

// function getId (req, res, next) {
//   upload.getId(req.params, (err, content) => {
//     if (err) res.status(500).send(err)
//     res.json(content)
//   })
// }

// function getTypes (req, res, next) {
//   upload.getTypes(req.query, (err, content) => {
//     if (err) res.status(500).send(err)
//     res.json(content)
//   })
// }

// function getCategoryTypes(req, res, next) {
//   upload.getCategoryTypes((error, data) => {
//     if (error) return res.status(error.status).json(error)
//     return res.json(data)
//   })
// }
function search(req, res, next) {
  upload.search(req.body, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.json(data)
  })
}

// function getCompanyTypes (req, res, next) {
//   upload.getCompanyTypes(req.params, (err, content) => {
//     if (err) res.status(500).send(err)
//     res.json(content)
//   })
// }

// function getCompanyTypesItens (req, res, next) {
//   upload.getCompanyTypesItens(req.params, (err, content) => {
//     if (err) res.status(500).send(err)
//     res.json(content)
//   })
// }

function post (req, res, next) {
  req.body.files = req.files
  
  upload.post (req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

// function putId (req, res, next) {
//   upload.putId(req.params, req.body, (err, data) => {
//     if (err) res.status(500).send(err)
//     res.json(data)
//   })
// }

// function patchId (req, res, next) {
//   req.body.file = req.file

//   upload.patchId(req.params, req.body, (err, data) => {
//     if (err) res.status(500).send(err)
//     res.json(data)
//   })
// }

function deleteId(req, res, next){
  upload.deleteId(req.params, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

// function getMinimumItemTypesFromUnities(req, res) {
//   upload.getMinimumItemTypesFromUnities(req.params.itemTypeId, req.params.unityId, req.query, (error, data) => {
//     if (error) return res.status(error.status).json(error)
//     return res.json(data)
//   })
// }

// function updateMinimumItemTypesFromUnity(req, res) {
//   upload.updateMinimumItemTypesFromUnity(req.params.itemTypeId, req.params.unityId, req.body, (error, data) => {
//     if (error) return res.status(error.status).json(error)
//     return res.status(204).json(data)
//   })
// }

module.exports = {
  get,
  // getId,
  // getTypes,
  // getCategoryTypes,
  // getCompanyTypes,
  // getCompanyTypesItens,
  // getMinimumItemTypesFromUnities,
  post,
  search,
  // putId,
  // updateMinimumItemTypesFromUnity,
  // patchId,
  deleteId
}
