const express = require("express")
const uploadController = require("./uploadController")
const verifyToken = require('../middlewares/verify')
const upload = require('../middlewares/upload')

function init() {	  
	let routes = express.Router()

	routes.route('/')
		.get(uploadController.get)
		.post(upload.fields([{
				name: 'image',
				maxCount: 1
			}, {
				name: 'gallery',
				maxCount: 5
		}]), uploadController.post)
		
	// routes.route('/types')
	// 	.get(verifyToken, uploadController.getTypes)

	// routes.route('/category')
	// 	.get(uploadController.getCategoryTypes)
	routes.route('/search')
		.post(uploadController.search)
	// routes.route('/types/companytypes/:id')
	// 	.get(verifyToken, uploadController.getCompanyTypes)
	
	// routes.route('/types/company/:id/types/:type')
	// 	.get(verifyToken, uploadController.getCompanyTypesItens)

	// routes.route('/:itemTypeId/unities/:unityId')
	// 	.get(verifyToken, uploadController.getMinimumItemTypesFromUnities)
	// 	.put(verifyToken, uploadController.updateMinimumItemTypesFromUnity)

  	routes.route('/:id')
	// 	.get(verifyToken, uploadController.getId)
	// 	.put(verifyToken, uploadController.putId)
	// 	.patch(verifyToken, upload.single('datasheet'), uploadController.patchId)
		.delete(uploadController.deleteId)
	return routes
}

module.exports = {init}
