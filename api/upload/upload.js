const mysql = require('mysql')
// const multer = require('multer')
const config = require('../../config')

var pool = mysql.createPool(config.mysqlConfig)

function get(callback) {
  // Listar todos os tipo item
  let query = ` SELECT * FROM img_upload `
  pool.getConnection((err, connection) => {
    connection.query(query, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      let typeItens = results
      return callback(null, typeItens)
    })
  })
}


function post(data, callback) {
  // console.log(data.files['image'][0]);
  var img_path = data.files['image'][0];
  img_path.path = img_path.path.replace(/\\/g, '/');
  img_path = 'http://localhost:3000/' + img_path.path
  // console.log('CRIOU PARAM: ' + JSON.stringify(values));
  let query = 'INSERT INTO img_upload SET ?'
  let values = [{
    img_upload_path: img_path
  }]
  pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, {
        success: true
      })
    })
  })
}

function search(data, callback) {
  // Listar todos os tipo item
  let query = "SELECT * FROM img_upload WHERE img_upload_path LIKE '%" + data.searchText + "%'";
  // let values = [{
  //   img_upload_path: '%' + data.searchText + '%'
  // }]
  //console.log(values)

  pool.getConnection((err, connection) => {
    console.log(query)
    connection.query(query, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      let typeItens = results
      return callback(null, typeItens)
    })
  })
}

// function putId(param, data, callback) {
//   // Atualizar informações da tipo item por ID
//   let query = 'UPDATE tii_tipo_item SET ? WHERE ?'
//   let values = [
//     {
//       tii_descricao: data.tii_descricao,
//       tii_valor: data.tii_valor,
//       tii_valor_emergencial: data.tii_valor_emergencial,
//       tii_medidas: data.tii_medidas,
//       tii_peso_liquido: data.tii_peso_liquido,
//       tii_peso_bruto: data.tii_peso_bruto,
//       tii_tipo: data.tii_tipo,
//       tii_arquivo: data.tii_arquivo,
//       tii_emp_empresa_id: data.tii_emp_empresa_id,
//       cat_categoria_id: data.cat_categoria_id
//     },
//     { tii_tipo_item_id: param.id }
//   ]

//   pool.getConnection((err, connection) => {
//     connection.query(query, values, (error, results, fields) => {
//       connection.release()
//       if (error) return callback(error)
//       return callback(null, { success: true })
//     })
//   })
// }

// function patchId(param, data, callback) {
//   if (data.file)
//     data.tii_arquivo = data.file.path.replace(/\\/g, '/')

//   delete data.file

//   let query = 'Update tii_tipo_item SET ? WHERE ?'
//   let values = [data, { tii_tipo_item_id: param.id }]

//   pool.getConnection((err, connection) => {
//     connection.query(query, values, (error, results, fields) => {
//       connection.release()
//       if (error) return callback(error)
//       return callback(null, { success: true })
//     })
//   })
// }

function deleteId(param, callback) {
  // deletar o tipo item
  let query = 'DELETE FROM img_upload WHERE ?'
  let values = [{
    img_upload_id: param.id
  }]

  pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, {
        success: true
      })
    })
  })
}

// function getMinimumItemTypesFromUnities(itemTypeId, unityId, params, callback) {
//   pool.getConnection((error, conn) => {
//     if (error)
//       return callback({ message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500, error })

//     const sql = `
// 									SELECT 
// 										tii.tii_descricao, tiu.tiu_quant_min
// 									FROM tiu_tipo_item_unidade tiu 
// 									JOIN tii_tipo_item tii 
// 										ON tiu.tiu_tii_tipo_item_id = tii.tii_tipo_item_id 
// 									JOIN uni_unidade uni 
// 										ON tiu.tiu_uni_unidade_id = uni.uni_unidade_id 
// 									WHERE tiu.tiu_uni_unidade_id = ?
// 										AND tii.tii_tipo_item_id = ?
// 								`
//     const values = [unityId, itemTypeId]
//     conn.query(sql, values, (error, results, fields) => {
//       conn.release()
//       if (error) return callback({ status: 400, error })
//       return callback(null, results)
//     })
//   })
// }

// function updateMinimumItemTypesFromUnity(itemTypeId, unityId, data, callback) {
//   pool.getConnection((error, conn) => {
//     if (error)
//       return callback({ message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500, error })

//     const sql = `
// 									UPDATE tiu_tipo_item_unidade tiu 
// 									SET tiu.tiu_quant_min = ?
// 									WHERE tiu.tiu_tii_tipo_item_id = ? 
// 										AND tiu.tiu_uni_unidade_id = ?
// 								`
//     const values = [data.tiu_quant_min, itemTypeId, unityId]
//     conn.query(sql, values, (error, results, fields) => {
//       conn.release()
//       if (error) return callback({ status: 400, error })
//       return callback(null, null)
//     })
//   })
// }

module.exports = {
  get,
  // getId,
  // getTypes,
  // getCategoryTypes,
  // getCompanyTypes,
  // getCompanyTypesItens,
  // getMinimumItemTypesFromUnities,
  post,
  search,
  // putId,
  // updateMinimumItemTypesFromUnity,
  // patchId,
  deleteId
}