const mysql = require('mysql')
const config = require('../../config')
var pool = mysql.createPool(config.mysqlConfig)

function get (callback) {
	// Listar todos os telefones
  let query = 'SELECT tel_telefone_id, tel_ddd, tel_telefone, tel_ramal, tel_pes_pessoa_id, tel_uni_unidade_id from tel_telefone'
    
	pool.getConnection((err, connection) => {
		connection.query(query, (error, results, fields) => {
			connection.release()
			if (error) return callback(error)
			return callback(null, results)
		})
	})
}

function getId (param, callback) {
	// Buscar telefone por id
	let query = 'SELECT tel_telefone_id, tel_ddd, tel_telefone, tel_ramal, tel_pes_pessoa_id, tel_uni_unidade_id from tel_telefone where ?'
	let values = [{tel_telefone_id: param.id}]

	pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release()
			if (error) return callback(error)
			return callback(null, results)
		})
	})
}

function getUserId (param, callback) {
	// Buscar telefone por id de usuário
	let query = 'SELECT tel_telefone_id, tel_ddd, tel_telefone, tel_ramal, tel_pes_pessoa_id, tel_uni_unidade_id from tel_telefone where ?'
	let values = [{tel_pes_pessoa_id: param.id}]

	pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release()
			if (error) return callback(error)
			return callback(null, results)
		})
	})
}

function getUnityId (param, callback) {
	// Buscar telefone por id de unidade
	let query = 'SELECT tel_telefone_id, tel_ddd, tel_telefone, tel_ramal, tel_pes_pessoa_id, tel_uni_unidade_id from tel_telefone where ?'
	let values = [{tel_uni_unidade_id: param.id}]

	pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release()
			if (error) return callback(error)
			return callback(null, results)
		})
	})
}

function post(data, callback) {
	// Incluir novo telefone
  let query = 'INSERT INTO tel_telefone SET ?'
  let values = [
		{
			tel_ddd: data.tel_ddd, 
			tel_telefone: data.tel_telefone, 
			tel_ramal: data.tel_ramal,
			tel_pes_pessoa_id: data.tel_pes_pessoa_id,
			tel_uni_unidade_id: data.tel_uni_unidade_id
		}
	]
  
  pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release()
			if (error) return callback(error)
			return callback(null, {success: true})
		})
	})
}

function putId(param, data, callback) {
  // Atualizar telefone por id
  let query = 'UPDATE tel_telefone SET ? WHERE ?'
  let values = [
		{
			tel_ddd: data.tel_ddd, 
			tel_telefone: data.tel_telefone, 
			tel_ramal: data.tel_ramal,
			tel_pes_pessoa_id: data.tel_pes_pessoa_id,
			tel_uni_unidade_id: data.tel_uni_unidade_id
		},
		{
			tel_telefone_id: param.id
		}
	]
  
  pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release()
			if (error) return callback(error)
			return callback(null, {success: true})
		})
	})
}

function deleteId(param, callback) {
  // Excluir o telefone
	let query = 'DELETE FROM tel_telefone WHERE ?'
	let values = [{tel_telefone_id: param.id}]
  
  pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
    	connection.release()
      if (error) return callback(error)
      return callback(null, {success: true})
    })
  })
}

module.exports = {
	get,
	getId,
	getUserId,
	getUnityId,
	post,
	putId,
	deleteId
}