const telephones = require('./telephones')

function get (req, res, next) {
  telephones.get((err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getId (req, res, next) {
  telephones.getId(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getUserId (req, res, next) {
  telephones.getUserId(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getUnityId (req, res, next) {
  telephones.getUnityId(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function post (req, res, next) {
  telephones.post (req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function putId (req, res, next) {
  telephones.putId(req.params, req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function deleteId(req, res, next){
  telephones.deleteId(req.params, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

module.exports = {
  get,
  getId,
  getUserId,
  getUnityId,
  post,
  putId,
  deleteId
}
