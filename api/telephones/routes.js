const express = require("express")
const telephonesController = require("./telephonesController")
const verifyToken = require('../middlewares/verify')

function init() {
	let routes = express.Router()

	routes.route('/')
    .get(telephonesController.get)
		.post(verifyToken, telephonesController.post)

	routes.route('/user/:id')
		.get(verifyToken, telephonesController.getUserId)

	routes.route('/unity/:id')
		.get(verifyToken, telephonesController.getUnityId)
        
	routes.route('/:id/')
		.get(verifyToken, telephonesController.getId)
		.put(verifyToken, telephonesController.putId)
		.delete(verifyToken, telephonesController.deleteId)

	return routes
}

module.exports = {init}