const express = require("express")
const projectsController = require("./projectsController")
// const verifyToken = require('../middlewares/verify')

function init() {
	let routes = express.Router()

	routes.route('/')
		.get(projectsController.get)
		.post(projectsController.post)

	// routes.route('/types')
	// 	.get(verifyToken, projectsController.getType)

	routes.route('/:id')
		.delete(projectsController.deleteId)
		.get(projectsController.getId)
		.put(projectsController.putId)

  return routes

}

module.exports = {
	init
}