const mysql  = require('mysql')
const config = require('../../config')

var pool = mysql.createPool(config.mysqlConfig)

function get (callback) {
  // Listar todas as empresas
  let query = 'SELECT * FROM pro_project'
  
  pool.getConnection((err, connection) => {
		connection.query(query, (error, results, fields) => {
			connection.release()
      if (error) return callback(error)
      return callback(null, results)
		})
  })
}

function getId (params, callback) {
	// Buscar informações de uma empresa
	let query = 'SELECT * FROM pro_project WHERE ?'
  let values = [{pro_project_id: params.id}]
	pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
	    connection.release()
  		if (error) return callback(error)
			return callback(null, results)
		})
	})
}

// function getType (params, callback) {
// 	// Listar por tipo
// 	let query = `SELECT emp_empresa_id, emp_nome, emp_status, emp_created_by, emp_created_at FROM emp_empresa WHERE ?`
//   let values = [{emp_tipo: params.type}]
//   pool.getConnection((err, connection) => {
// 		connection.query(query, values, (error, results, fields) => {
// 			connection.release()
// 			if (error) return callback(error)
// 			return callback(null, results)			
// 		})
// 	})
// }

function post(data, callback) {
  // Inserir empresa
  let query = 'INSERT INTO pro_project SET ?'
  let values = [{...data}]
  
  pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
			return callback(null, {success: true})
		})
  })
}

function deleteId(param, data, callback) {
  // Desativar a unidade
  console.log(data.status)
  let query = 'UPDATE pro_project SET ? WHERE ?'
  let values = [{
    pro_status: data.status},
    {pro_project_id: param.id
  }]

  pool.getConnection((err, connection) => {
      connection.query(query, values, (error, results, fields) => {
          connection.release()
          if (error) return callback(error)
          return callback(null, {
              success: true
          })
      })
  })
}

function putId(params, data, callback) {
  // Atualizar informações da empresa por ID
  let query = 'UPDATE pro_project SET ? WHERE ?'
  let values = [{...data}, {pro_project_id: params.id}]
    
  pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, {success: true})
    })
  })
}

module.exports = {
  get,
  getId,
  // getType,
  post,
  deleteId,
  putId
}