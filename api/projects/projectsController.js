const projects = require('./projects')

function get(req, res, next) {
  projects.get((err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getId(req, res, next) {
  projects.getId(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

// function getType(req, res, next) {
//   projects.getType(req.query, (err, content) => {
//     if (err) res.status(500).send(err)
//     res.json(content)
//   })
// }

function post(req, res, next) {
  projects.post(req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function deleteId(req, res, next) {
  projects.deleteId(req.params, req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function putId(req, res, next) {
  projects.putId(req.params, req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

module.exports = {
  get,
  getId,
  // getType,
  post,
  deleteId,
  putId
}