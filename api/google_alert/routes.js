const express = require("express")
const googleAlertController = require("./googleAlertController")
const verifyToken = require('../middlewares/verify')

function init() {
	let routes = express.Router()

	routes.route('/')
		.get(googleAlertController.get)
		.post(verifyToken, googleAlertController.post)

	routes.route('/:id')
    .put(verifyToken, googleAlertController.put)
    .delete(verifyToken, googleAlertController.deleteId)

	routes.route('/news')
    .get(googleAlertController.getNews)

	return routes
}

module.exports = {init}