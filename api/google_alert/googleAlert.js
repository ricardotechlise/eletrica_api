const mysql = require('mysql')
const config = require('../../config')
const axios = require('axios')
const parseString = require('xml2js').parseString
var pool = mysql.createPool(config.mysqlConfig)

function get(callback) {
	let query = 'SELECT * FROM goa_google_alertas'
	pool.getConnection((err, connection) => {
		connection.query(query, (error, results, fields) => {
			connection.release()
			if (error) {
				return callback(error)
			} else {
				var urlList = [];
				var entries = []
				results.map(data => {
					urlList = [...axios.get(data.url)]
				})
				urlList.map((response, index) => {
					parseString(response.data, (err, result) => {
						if (index == responses.length - 1) entries = [...entries, ...result.feed.entry]
					})
				})
				return callback(null, entries)
			}
		})
	})
}

function getNews(callback) {
	let query = 'SELECT * FROM goa_google_alertas'

	pool.getConnection((err, connection) => {
		connection.query(query, (error, results, fields) => {
			connection.release()
			if (error) return callback(error)
			return callback(null, results)
		})
	})
}

function post(data, callback) {
	let query = 'INSERT INTO goa_google_alertas SET ?'
	let values = [{
		url: data.url
	}]

	pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release()
			if (error) return callback(error)
			return callback(null, {})
		})
	})
}

function put(id, data, callback) {
	let query = 'UPDATE goa_google_alertas SET url = ? WHERE id = ?'
	let values = [data.url, id]

	pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release()
			if (error) return callback(error)
			return callback(null, {
				success: true
			})
		})
	})
}

function deleteId(id, callback) {
	let query = 'DELETE FROM goa_google_alertas WHERE id = ?'
	let values = [id]

	pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release()
			if (error) return callback(error)
			return callback(null, {
				success: true
			})
		})
	})
}

module.exports = {
	get,
	getNews,
	post,
	put,
	deleteId
}