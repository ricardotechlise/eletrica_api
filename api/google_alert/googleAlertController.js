const googleAlert = require('./googleAlert.js')

function get (req, res, next) {
  googleAlert.get((err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getNews (req, res, next) {
  googleAlert.getNews((err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function post (req, res, next) {
  googleAlert.post(req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function put (req, res, next) {
  googleAlert.put(req.params.id, req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function deleteId (req, res, next) {
  googleAlert.deleteId(req.params.id, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}


module.exports = {
  get,
  getNews,
  post,
  put,
  deleteId
}
