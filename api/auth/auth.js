const mysql = require('mysql')
const express = require('express')
const jwt = require('jsonwebtoken')
const nodemailer = require('nodemailer')
const bcrypt = require('bcryptjs')
const uuid = require('uuid/v4')
const config = require('../../config')

const app = express()
const pool = mysql.createPool(config.mysqlConfig)

function post(data, callback) {
  let usu = data.usu
  let usr_senha = data.usr_senha
  
  let query = 'SELECT usr.usr_user_id, usr.usr_tipo, usr.usr_senha, usr.usr_status, pes.pes_uni_unidade_id, pes.pes_nome, pes.pes_cpf, pes.pes_email FROM usr_user usr LEFT JOIN pes_pessoa pes ON usr.usr_user_id = pes.pes_usr_user_id_acesso WHERE ?'
  let values = {usr_apelido: usu}
  
  pool.getConnection((err, connection) => {
    if (err) return callback(err)
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (err) {
        return callback(err)
      } else if (results.length > 0) {
        let result = results[0]
          
        const passwordIsValid = bcrypt.compareSync(data.usr_senha, result.usr_senha)

        if (!passwordIsValid)
          return callback({status: false, error: 'Usuário e/ou senha inválidos.'})

        if (result.usr_status == 0) {
          return callback(null, {status: false, error: 'Usuário inativo. Entre em contato com a administração.'})
        } else {
          app.set('superSecret', config.secret)
          var token = jwt.sign(result, app.get('superSecret'), {})          
          return callback(null, {status: true, token})      
        }
      } else {
        return callback(null, {status: false, error: 'Usuário e/ou senha inválidos.'})
      }
    })
  })
}

function get(token, callback) {
  if (!token) {
    return callback(null, {auth: false, error: 'Não foi passado um token.', status: 403})
  }

  token = token.split(' ')
  if (token[0] !== 'Bearer')
    return callback(null, {auth: false, error: 'Token não autenticado.', status: 401})

  token = token[1]
  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) return callback({auth: false, error: 'Token não autenticado.', status: 401}) 
    delete decoded.usr_senha
    delete decoded.iat
    return callback(null, decoded)
  })
}

function forgotPassword(data, callback) {
  pool.getConnection((error, conn) => {
    if (error) 
      return callback({message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500, error})

    conn.beginTransaction(error => {
      if (error) {
        conn.rollback()
        return callback({status: 500, error})
      }

      const recoveryId = uuid()

      let sql = `UPDATE usr_user SET usr_id_recuperacao = ?, usr_recuperacao_requisicao_tempo = ? WHERE usr_apelido = ?`
      let params = [recoveryId, new Date(), data.usr_apelido]

      conn.query(sql, params, (error, results, fields) => {
        if (error) {
          conn.rollback()
          return callback({status: 500, error})
        }

        sql = `
                SELECT 
                  p.pes_email 
                FROM pes_pessoa p 
                JOIN usr_user u 
                  ON p.pes_usr_user_id_acesso = u.usr_user_id
                WHERE u.usr_apelido = ?
              `
        params = [data.usr_apelido]

        conn.query(sql, params, (error, results, fields) => {
          if (error) {
            conn.rollback()
            return callback({status: 500, error})
          }

          if (results.length < 1) {
            conn.rollback()
            return callback(null, null)
          }

          const email = results[0].pes_email

          const transporter = nodemailer.createTransport(config.transporter)
          const mailOptions = {
            from: config.transporter.auth.user,
            to: email,
            subject: 'Troca de senha EcoResponse',
            html: `
              <p>Acesse o link abaixo para trocar a senha da sua conta no EcoResponse:</p>
              <br/>
              <a href="http://ecoresponse.techlise.com.br/reset-password/${recoveryId}">www.ecoresponse.techlise.com.br/reset-password/${recoveryId}</a>
            `
          }

          transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
              conn.rollback()
              return callback({message: 'Não foi possível enviar o email ao destinatário.', status: 500, error})
            }

            conn.commit(error => {
              if (error) {
                conn.rollback()
                return callback({status: 500, error})
              }
              return callback(null, null)
            })
          })
        })
      })
    })
  })
}

function resetPassword(recoveryId, data, callback) {
  if (data.usr_senha < 6)
    return callback({message: 'A senha deve conter no minímo 6 caracteres.', status: 422})

  pool.getConnection((error, conn) => {
    if (error)
      return callback({message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500, error})
  
    let sql = `SELECT usr_recuperacao_requisicao_tempo FROM usr_user WHERE usr_id_recuperacao = ?`
    let params = recoveryId

    conn.query(sql, params, (error, results, fields) => {
      if (error) {
        conn.release()
        return callback({status: 500, error})
      }

      if (results.length < 1) {
        conn.release()
        return callback({status: 400, message: 'Id de recuperação inválido.'})
      }

      const recoveryRequestTime = results[0].usr_recuperacao_requisicao_tempo
      const recoveryExpiration = new Date(recoveryRequestTime.setHours(recoveryRequestTime.getHours() + 24))
      const actualDate = new Date()

      if (actualDate > recoveryExpiration) 
        return callback({message: 'O tempo de troca de senha expirou.', status: 403})

      sql = `UPDATE usr_user SET usr_senha = ? WHERE usr_id_recuperacao = ?`
      params = [bcrypt.hashSync(data.usr_senha, 8), recoveryId]

      conn.query(sql, params, (error, results, fields) => {
        conn.release()
        if (error) return callback({status: 500, error})
        return callback(null, null)
      })
    })
  })
}

module.exports = { post, get, forgotPassword, resetPassword }
