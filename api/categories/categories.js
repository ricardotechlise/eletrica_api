const mysql = require('mysql')
const config = require('../../config')

const pool = mysql.createPool(config.mysqlConfig)

function createCategory(data, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({error, message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500})

    const sql = `
                  INSERT INTO cat_categoria SET ?
                `
    const values = {
      cat_descricao: data.cat_descricao,
      cat_created_by: data.cat_created_by
    }

    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) return callback({error, status: 400})
      return callback(null, {id: results.insertId})
    })
  })
}

function getCategories(params, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({error, message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500})

    let sql = `
                SELECT * FROM cat_categoria
              `
    let values = []

    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) return callback({error, status: 400})
      return callback(null, results)
    })
  })
}

function updateCategory(id, data, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({error, message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500})

    const sql = `
                  UPDATE cat_categoria SET ? WHERE cat_categoria_id = ?
                `
    const values = [{
      cat_descricao: data.cat_descricao
    }, id]

    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) return callback({error, status: 400})
      return callback(null, null)
    })
  })
}

function changeCategory(id, data, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({error, message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500})

    const sql = `
                  UPDATE cat_categoria SET ? WHERE cat_categoria_id = ?
                `
    const values = [{
      cat_status: data.cat_status
    }, id]

    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) return callback({error, status: 400})
      return callback(null, null)
    })
  })
}

module.exports = {
  createCategory,
  getCategories,
  updateCategory,
  changeCategory
}
