const categories = require('./categories')

function createCategory(req, res) {
  categories.createCategory(req.body, (error, data) => {
    if (error) return res.status(error.status).json(error)
    res.status(201).json(data)
  })
}

function getCategories(req, res) {
  categories.getCategories(req.query, (error, data) => {
    if (error) return res.status(error.status).json(error)
    res.json(data)
  })
}

function updateCategory(req, res) {
  categories.updateCategory(req.params.id, req.body, (error, data) => {
    if (error) return res.status(error.status).json(error)
    res.status(204).json(data)
  })
}

function changeCategory(req, res) {
  categories.changeCategory(req.params.id, req.body, (error, data) => {
    if (error) return res.status(error.status).json(error)
    res.status(204).json(data)
  })
}

module.exports = {
  createCategory,
  getCategories,
  updateCategory,
  changeCategory
}
