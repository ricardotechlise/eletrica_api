const express = require('express')
const verifyToken = require('../middlewares/verify')
const categoriesController = require('./categoriesController')

function init(){
  const routes = express.Router()

  routes.route('/')
    .post(verifyToken, categoriesController.createCategory)
    .get(verifyToken, categoriesController.getCategories)

  routes.route('/:id')
    .put(verifyToken, categoriesController.updateCategory)
    .patch(verifyToken, categoriesController.changeCategory)

  return routes
}

module.exports = { init }
