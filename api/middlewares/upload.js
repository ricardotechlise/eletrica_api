const multer = require('multer')
const fs = require('fs')
const path = require('path')

function getDate() {
  let date = new Date()

  date = date.getUTCFullYear() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCDate()
  
  return date
}

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
   // console.log(req.body);
    const dir = './public/uploads/' + getDate() + '/'
    fs.mkdir(dir, err => {
      if (err && err.code !== 'EEXIST') return cb(err)
      cb(null, dir)
    })
  },
 // filename: (req, file, cb) => cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
  filename: (req, file, cb) => cb(null, file.originalname)
})

module.exports = upload = multer({
  storage,
  limits: { fileSize: 1000000 },
  fileFilter: (req, file, cb) => {
    const fileTypes = /jpeg|jpg|png|pdf/
    const extName = fileTypes.test(path.extname(file.originalname).toLowerCase())
    const mimeType = fileTypes.test(file.mimetype)
    // console.log(req.files);
    if (!mimeType && !extName) return cb({ message: 'O servidor aceita apenas arquivos do tipo: pdf, jpg, png.' })
    cb(null, true)
  }
})
