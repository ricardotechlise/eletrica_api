const jwt = require('jsonwebtoken')
const config = require('../../config')

module.exports = function verifyToken(req, res, next) {
  let token = req.headers['authorization']
  if (!token) return res.status(403).json({auth: false, error: 'Não foi passado um token.'})

  token = token.split(' ')
  if (token[0] !== 'Bearer') return res.status(401).json({auth: false, error: 'Token não autenticado.'})

  token = token[1]
  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) return res.status(401).json({auth: false, error: 'Token não autenticado.'})

    req.decoded = decoded
    next()
  })
}
