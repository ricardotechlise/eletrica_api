const item = require('./item')

function getByHash (req, res, next) {
  item.getByHash(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function post (req, res, next) {
  item.post (req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function putId (req, res, next) {
  item.putId(req.params.id, req.body, (err, data) => {
      if (err) res.status(500).send(err)
      else {
        res.json(data)
      }
  })
}

function deleteId(req, res, next){
  item.deleteId(req.params.id, (err, data) => {
    if (err) res.status(500).send(err)
    else {
      res.status(204).json(data)
    }
  })
}

function getId (req, res, next) {
  item.getId(req.params.id, req.query, (err, content) => {
    if (err) {
      res.status(500).send(err)
    } else {
      // console.log(res)
      res.json(content)
    }
  })
}

module.exports = {
  getByHash,
  post,
  putId,
  deleteId,
  getId
}
