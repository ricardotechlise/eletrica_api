const express = require("express")
const itemController = require("./itemController")
const verifyToken = require('../middlewares/verify')

function init() {
	let routes = express.Router()

	routes.route('/')
		.post(verifyToken, itemController.post)
	
	routes.route('/hash/:hash')
		.get(verifyToken, itemController.getByHash)
        
	routes.route('/:id/')
		.get(verifyToken, itemController.getId)
		.put(verifyToken, itemController.putId)
		.delete(verifyToken, itemController.deleteId)

	return routes

}

module.exports = {init}