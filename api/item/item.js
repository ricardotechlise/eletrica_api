const mysql = require('mysql')
const config = require('../../config')
const crypto = require('crypto')
var algorithm = 'aes-256-ctr'
var password = 'ecoresponse'
var pool = mysql.createPool(config.mysqlConfig)

function getByHash(param, callback) {
  let hash = param.hash

  let query = 'SELECT * from ite_item WHERE ?'
  let values = [{ ite_qrcode: hash }]

  pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) {
        return callback(error)
      } else {
        var decipher = crypto.createDecipher(algorithm, password)
        var dec = decipher.update(hash, 'hex', 'utf8')
        dec += decipher.final('utf8')

        var newHash = JSON.parse(dec)

        let response = {
          type: newHash.type,
          data: results
        }
        return callback(null, response)
      }
    })
  })
}

function post(data, callback) {

  if (!data.quantity || isNaN(data.quantity)) return callback({ message: 'Não foi passada a quantidade de itens.' })

  pool.getConnection((error, connection) => {
    if (error) return callback(error)

    connection.beginTransaction(error => {
      if (error) {
        connection.rollback()
        return callback(error)
      }

      const createItems = new Promise((resolve, reject) => {
        let items = []

        for (let i = 0; i < data.quantity; i++) {
          let sql = `INSERT INTO ite_item SET ?`
          let params = {
            ite_uni_unidade_id: data.ite_uni_unidade_id,
            ite_tii_tipo_item_id: data.ite_tii_tipo_item_id,
            ite_created_by: data.ite_created_by
          }

          connection.query(sql, params, (error, results, fields) => {
            if (error) return reject(error)

            items = items.concat({id: results.insertId})

            const kitItemId = data.type == 'K' ? results.insertId : null
            let cipher = crypto.createCipher(algorithm, password)
            let obj = {
              ite_item_id: kitItemId,
              type: data.type
            }
            let crypted = cipher.update(JSON.stringify(obj), 'utf8', 'hex')
            crypted += cipher.final('hex')

            sql = `UPDATE ite_item SET ite_qrcode = ? WHERE ite_item_id = ?`
            params = [crypted, kitItemId]

            connection.query(sql, params, (error, results, fields) => {
              if (error) return reject(error)
              if (kitItemId) {
                sql = `
                        SELECT 
                          tik_tipo_item_id_kit, tik_tipo_item_item, tii.tii_descricao, 
                          tik_quant, tik_status, tik_created_by, 
                          tik_created_at 
                        FROM tik_tipo_item_kit 
                        JOIN tii_tipo_item tii 
                          ON tii.tii_tipo_item_id = tik_tipo_item_item
                        WHERE ?
                      `
                params = {tik_tipo_item_id_kit: data.ite_tii_tipo_item_id}
                connection.query(sql, params, (error, results, fields) => {
                  if (error) return reject(error)

                  const itemsKit = results.filter(itemKit => itemKit.tik_status == 1)

                  itemsKit.forEach((itemKit, index) => {
                    for (let i = 0; i < itemKit.tik_quant; i++) {
                      const createItemsKit = new Promise((resolve, reject) => {
                        sql = `INSERT INTO ite_item SET ?`
                        params = {
                          ite_uni_unidade_id: data.ite_uni_unidade_id,
                          ite_tii_tipo_item_id: itemKit.tik_tipo_item_item,
                          ite_created_by: data.ite_created_by
                        }
                        connection.query(sql, params, (error, results, fields) => {
                          if (error) return reject(error)

                          const itemId = results.insertId
                          cipher = crypto.createCipher(algorithm, password)
                          obj = {
                            ite_item_id: itemId,
                            type: 'I'
                          }
                          crypted = cipher.update(JSON.stringify(obj), 'utf8', 'hex')
                          crypted += cipher.final('hex')

                          sql = `UPDATE ite_item SET ite_qrcode = ? WHERE ite_item_id = ?`
                          params = [crypted, itemId]

                          connection.query(sql, params, (error, results, fields) => {
                            if (error) return reject(error)

                            sql = `INSERT INTO itk_item_kit SET ?`
                            params = {
                              itk_ite_item_id_kit: kitItemId,
                              itk_ite_item_id_item: itemId,
                              itk_created_By: data.ite_created_by
                            }
                            connection.query(sql, params, (error, results, fields) => {
                              if (error) return reject(error)
                              if (i == itemKit.tik_quant - 1) resolve()
                            })
                          })
                        })
                      })

                      createItemsKit
                      .then(() => {
                        if (index == itemsKit.length - 1) resolve(items)
                      })
                      .catch(error => reject(error))
                    }
                  })
                })
              } else if (i === data.quantity - 1) resolve(items)
            })
          })
        }
      })

      createItems
      .then(items => connection.commit(error => error ? callback(error) : callback(null, items)))
      .catch(error => {
        connection.rollback()
        return callback(error)
      })
    })
  })
}

// 
function putId(route, data, callback) {

  // Atualizar informações do item por ID

  let query = 'UPDATE ite_item SET ? WHERE ?'
  let values = ''

  let ite_item_id = data.ite_item_id
  let ite_status = null

  switch (route) {
    case '1':

      // Alterar informaçoes do item

      let ite_tii_tipo_item_id = data.ite_tii_tipo_item_id
      let ite_qrcode = data.ite_qrcode

      values = [{ ite_qrcode: ite_qrcode, ite_tii_tipo_item_id: ite_tii_tipo_item_id }, { ite_item_id: ite_item_id }]
      break;

    case '2':

      // Altera o status do item para disponivel

      ite_status = 1

      values = [{ ite_status: ite_status }, { ite_item_id: ite_item_id }]
      break;

    case '3':

      // Altera status para transferencia

      let ite_uni_unidade_id = data.ite_uni_unidade_id
      ite_status = 2

      values = [{ ite_uni_unidade_id: ite_uni_unidade_id, ite_status: ite_status }, { ite_item_id: ite_item_id }]
      break;
  }

  pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, { success: true })
    })
  })
}

function deleteId(ite_item_id, callback) {

  // Desativar o item
  pool.getConnection((err, conn) => {
    if (err) return callback(err)
    let sql = `
                SELECT 
                  itk_ite_item_id_item
                FROM itk_item_kit
                WHERE itk_ite_item_id_kit = ?
              `
    conn.query(sql, ite_item_id, (error, results, fields) => {
      if (error) return callback(error)

      if (results.length > 0) {
        conn.beginTransaction(error => {
          if (error) {
            conn.rollback()
            return callback(error)
          }

          const itemsKit = results.map(itemKit => itemKit.itk_ite_item_id_item)

          sql = `UPDATE ite_item SET ite_status = 0 WHERE ite_item_id = ?`
          conn.query(sql, ite_item_id, (error, results, fields) => {
            if (error) {
              conn.rollback()
              return callback(error)
            }
            
            sql = `UPDATE itk_item_kit SET itk_status = 0 WHERE itk_ite_item_id_kit = ?`
            conn.query(sql, ite_item_id, (error, results, fields) => {
              if (error) {
                conn.rollback()
                return callback(error)
              }

              const deleteItemsKit = new Promise((resolve, reject) => {
                itemsKit
                .forEach((id, index) => {
                  sql = `UPDATE ite_item SET ite_status = 0 WHERE ite_item_id = ?`
                  conn.query(sql, id, (error, results, fields) => {
                    if (error) return reject(error)
                    if (index == itemsKit.length - 1) resolve()
                  })
                })
              })

              deleteItemsKit
              .then(() => conn.commit(err => err ? callback(err) : callback(null, null)))
              .catch(error => callback(error))
            })
          })
        })
      } else {
        sql = `UPDATE ite_item SET ite_status = 0 WHERE ite_item_id = ?`
        conn.query(sql, ite_item_id, (error, results, fields) => {
          conn.release()
          if (error) return callback(error)
          callback(null, null)
        })
      }
    })
  })

  let ite_status = 0

  let values = [{ ite_status: ite_status }, { ite_item_id: ite_item_id }]

  let query = 'UPDATE ite_item SET ? WHERE ?'

  pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, { success: true })
    })
  })
}

function getId(id, param, callback) {
  pool.getConnection((err, connection) => {
    if (err)
      return callback(err)

    const sql = `
                        SELECT 
                            tii.tii_descricao, tii.tii_tipo, tii.tii_arquivo, 
                            ite.ite_item_id, ite.ite_uni_unidade_id, uni.uni_nome_fantasia, 
                            ite.ite_tii_tipo_item_id, ite.ite_status, ite.ite_qrcode, 
                            ite.ite_created_by, ite.ite_created_at 
                        FROM ite_item ite 
                        JOIN tii_tipo_item tii 
                            ON ite.ite_tii_tipo_item_id = tii.tii_tipo_item_id 
                        JOIN uni_unidade uni 
                            ON ite.ite_uni_unidade_id = uni.uni_unidade_id 
                        WHERE ite.ite_item_id = ?
                    `
    connection.query(sql, id, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, results)
    })
  })
}

module.exports = {
  getByHash,
  post,
  putId,
  deleteId,
  getId
}