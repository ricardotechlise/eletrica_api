const mysql = require('mysql')
const multer = require('multer')
const config = require('../../config')

var pool = mysql.createPool(config.mysqlConfig)

function get(params, callback) {
  // Listar todos os tipo item
  let query = `
                SELECT 
                  t.tii_tipo_item_id, t.tii_descricao, t.tii_medidas, 
                  t.tii_peso_liquido, t.tii_peso_bruto, t.tii_valor, 
                  t.tii_valor_emergencial, t.tii_status, t.tii_tipo, 
                  t.tii_arquivo, t.tii_emp_empresa_id, t.tii_created_by, 
                  t.tii_created_at, t.tii_mid_midia_id_perfil, t.cat_categoria_id,
                  c.cat_descricao,
                  GROUP_CONCAT(
                          '{ "mid_midia_id": ', m.mid_midia_id,
                          ', "mid_href": "', m.mid_href,
                          '", "mid_tipo": "', m.mid_tipo,'" }'
                        ) AS 'mid_midia'
                FROM tii_tipo_item t
                LEFT JOIN mid_midia m
                  ON t.tii_tipo_item_id = m.mid_tii_tipo_item_id
                JOIN cat_categoria c
                  ON t.cat_categoria_id = c.cat_categoria_id
              `
  let values = []

  if (params.categoria) {
    query += `
                WHERE t.cat_categoria_id = ?
             `
    values = [...values, params.categoria]
  }

  query += `
            GROUP BY 
              t.tii_tipo_item_id, t.tii_descricao, t.tii_medidas, 
              t.tii_peso_liquido, t.tii_peso_bruto, t.tii_valor, 
              t.tii_valor_emergencial, t.tii_status, t.tii_tipo, 
              t.tii_arquivo, t.tii_emp_empresa_id, t.tii_created_by, 
              t.tii_created_at, t.tii_mid_midia_id_perfil
           `

  pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      let typeItens = results
      typeItens.map(typeItem => {
        typeItem.mid_midia = typeItem.mid_midia ? JSON.parse('[' + typeItem.mid_midia + ']') : []
        typeItem.perfil = typeItem.mid_midia.filter(midia => typeItem.tii_mid_midia_id_perfil == midia.mid_midia_id)[0] || {}
        typeItem.galeria = typeItem.mid_midia.filter(midia => typeItem.tii_mid_midia_id_perfil != midia.mid_midia_id)
        delete typeItem.mid_midia
        return typeItem
      })
      return callback(null, typeItens)
    })
  })
}

function getId(param, callback) {
  // Buscar informações do tipo item
  let query = `
								SELECT 
									t.tii_tipo_item_id, t.tii_descricao, t.tii_medidas, 
									t.tii_peso_liquido, t.tii_peso_bruto, t.tii_valor, 
									t.tii_valor_emergencial, t.tii_status, t.tii_tipo, 
									t.tii_arquivo, t.tii_emp_empresa_id, t.tii_created_by, 
									t.tii_created_at, t.tii_mid_midia_id_perfil, t.cat_categoria_id,
									GROUP_CONCAT(
													'{ "mid_midia_id": ', m.mid_midia_id,
													', "mid_href": "', m.mid_href,
													'", "mid_tipo": "', m.mid_tipo,'" }'
												) AS 'mid_midia'
								FROM tii_tipo_item t
								LEFT JOIN mid_midia m
									ON t.tii_tipo_item_id = m.mid_tii_tipo_item_id
								WHERE t.?
								GROUP BY 
									t.tii_tipo_item_id, t.tii_descricao, t.tii_medidas, 
									t.tii_peso_liquido, t.tii_peso_bruto, t.tii_valor, 
									t.tii_valor_emergencial, t.tii_status, t.tii_tipo, 
									t.tii_arquivo, t.tii_emp_empresa_id, t.tii_created_by, 
									t.tii_created_at, t.tii_mid_midia_id_perfil
							`
  let values = [{ tii_tipo_item_id: param.id }]

  pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, results)
    })
  })
}

function getTypes(param, callback) {
  // Buscar informações do tipo item
  let query = `
								SELECT 
									tii.tii_tipo_item_id, tii.tii_descricao, tii.tii_medidas, 
									tii.tii_peso_liquido, tii.tii_peso_bruto, tii.tii_valor, 
									tii.tii_valor_emergencial, tii.tii_status, tii.tii_tipo, 
									tii.tii_arquivo, tii.tii_emp_empresa_id, tii.tii_created_by, 
									tii.tii_created_at, tii.tii_mid_midia_id_perfil, tii.cat_categoria_id,
									GROUP_CONCAT(
													'{ "mid_midia_id": ', mid.mid_midia_id,
													', "mid_href": "', mid.mid_href,
													'", "mid_tipo": "', mid.mid_tipo,'" }'
												) AS 'mid_midia'
								FROM tii_tipo_item tii
								JOIN emp_empresa emp
									ON tii.tii_emp_empresa_id = emp.emp_empresa_id
								LEFT JOIN mid_midia mid
									ON tii.tii_tipo_item_id = mid.mid_tii_tipo_item_id
								WHERE 
									tii.? AND tii.tii_emp_empresa_id = emp.emp_empresa_id
								GROUP BY 
									tii.tii_tipo_item_id, tii.tii_descricao, tii.tii_medidas, 
									tii.tii_peso_liquido, tii.tii_peso_bruto, tii.tii_valor, 
									tii.tii_valor_emergencial, tii.tii_status, tii.tii_tipo, 
									tii.tii_arquivo, tii.tii_emp_empresa_id, tii.tii_created_by, 
									tii.tii_created_at, tii.tii_mid_midia_id_perfil
							`
  let values = [{ tii_tipo: param.type }]

  pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, results)
    })
  })
}

function getCategoryTypes(callback) {
  pool.getConnection((error, conn) => {
    if (error) return callback({ message: 'Falha ao estabelecer conexão com o banco de dados.', status: 500, error })

    let sql = `SELECT cat_categoria_id, cat_descricao FROM cat_categoria WHERE cat_status = 1`

    conn.query(sql, (error, results, fields) => {
      if (error) return callback({ error, status: 500 })

      const categories = results
      const getItemTypes = new Promise((resolve, reject) => {
        let response = [...categories]

        categories.forEach((category, index) => {
          sql =
            `		SELECT 
                    t.tii_tipo_item_id, t.tii_descricao, t.tii_medidas, 
                    t.tii_peso_liquido, t.tii_peso_bruto, t.tii_valor, 
                    t.tii_valor_emergencial, t.tii_status, t.tii_tipo, 
                    t.tii_arquivo, t.tii_emp_empresa_id, t.tii_created_by, 
                    t.tii_created_at, t.tii_mid_midia_id_perfil, t.cat_categoria_id,
                    GROUP_CONCAT(
                            '{ "mid_midia_id": ', m.mid_midia_id,
                            ', "mid_href": "', m.mid_href,
                            '", "mid_tipo": "', m.mid_tipo,'" }'
                          ) AS 'mid_midia'
                  FROM tii_tipo_item t
                  LEFT JOIN mid_midia m
                    ON t.tii_tipo_item_id = m.mid_tii_tipo_item_id
                  JOIN cat_categoria c
                    ON c.cat_categoria_id = t.cat_categoria_id
                  WHERE t.cat_categoria_id = ?
                  GROUP BY 
                    t.tii_tipo_item_id, t.tii_descricao, t.tii_medidas, 
                    t.tii_peso_liquido, t.tii_peso_bruto, t.tii_valor, 
                    t.tii_valor_emergencial, t.tii_status, t.tii_tipo, 
                    t.tii_arquivo, t.tii_emp_empresa_id, t.tii_created_by, 
                    t.tii_created_at, t.tii_mid_midia_id_perfil;
          `
          conn.query(sql, category.cat_categoria_id, (error, results, fields) => {
            if (error) return reject(error)

            let typeItens = results
            typeItens.map(typeItem => {
              typeItem.mid_midia = typeItem.mid_midia ? JSON.parse('[' + typeItem.mid_midia + ']') : []
              typeItem.perfil = typeItem.mid_midia.filter(midia => typeItem.tii_mid_midia_id_perfil == midia.mid_midia_id)[0] || {}
              typeItem.galeria = typeItem.mid_midia.filter(midia => typeItem.tii_mid_midia_id_perfil != midia.mid_midia_id)
              delete typeItem.mid_midia
              return typeItem
            })

            response[index].tii_tipo_items = typeItens
            if (index == categories.length - 1) return resolve(response)
          })
        })
      })

      getItemTypes
        .then(resp => callback(null, resp))
        .catch(error => callback({ status: 500, error }))
    })
  })
}

function getCompanyTypes(param, callback) {
  // Buscar informações do tipo item por empresa
  let query = `
								SELECT
									tii.tii_tipo_item_id, tii.tii_descricao, tii.tii_medidas, 
									tii.tii_peso_liquido, tii.tii_peso_bruto, tii.tii_valor, 
									tii.tii_valor_emergencial, tii.tii_status, tii.tii_tipo, 
									tii.tii_arquivo, tii.tii_emp_empresa_id, count(*) as tii_quant,
                  tii.tii_created_by, tii.tii_created_at, tii.tii_mid_midia_id_perfil,
                  tii.cat_categoria_id,
									GROUP_CONCAT(
										'{ "mid_midia_id": ', mid.mid_midia_id,
										', "mid_href": "', mid.mid_href,
										'", "mid_tipo": "', mid.mid_tipo,'" }'
									) AS 'mid_midia'
								FROM ite_item ite 
								LEFT JOIN itk_item_kit itk 
									ON ite.ite_item_id = itk.itk_ite_item_id_kit 
								LEFT JOIN tii_tipo_item tii 
									ON ite.ite_tii_tipo_item_id = tii.tii_tipo_item_id AND tii.tii_tipo = 'I' 
								LEFT JOIN mid_midia mid
									ON tii.tii_tipo_item_id = mid.mid_tii_tipo_item_id
								WHERE ite.ite_status = 1 AND itk.itk_ite_item_id_kit IS NULL 
									AND ite.ite_uni_unidade_id = ?
								GROUP BY 
									tii.tii_tipo_item_id, tii.tii_descricao, tii.tii_medidas, 
									tii.tii_peso_liquido, tii.tii_peso_bruto, tii.tii_valor, 
									tii.tii_valor_emergencial, tii.tii_status, tii.tii_tipo, 
									tii.tii_arquivo, tii.tii_emp_empresa_id, tii.tii_created_by, 
									tii.tii_created_at, tii.tii_mid_midia_id_perfil
							`
  let values = [param.id]

  pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      let typeItens = results
      typeItens.map(typeItem => {
        typeItem.mid_midia = typeItem.mid_midia ? JSON.parse('[' + typeItem.mid_midia + ']') : []
        let midias = []
        let auxMidias = []
        typeItem.mid_midia = typeItem.mid_midia.filter((midia, index) => {
          midias = [...auxMidias]
          auxMidias = [...midias, midia.mid_midia_id]
          return midias.indexOf(midia.mid_midia_id) == -1
        })
        typeItem.perfil = typeItem.mid_midia.filter(midia => typeItem.tii_mid_midia_id_perfil == midia.mid_midia_id)[0] || {}
        typeItem.galeria = typeItem.mid_midia.filter(midia => typeItem.tii_mid_midia_id_perfil != midia.mid_midia_id)
        delete typeItem.mid_midia
        return typeItem
      })
      return callback(null, typeItens)
    })
  })
}

function getCompanyTypesItens(param, callback) {
  // Buscar itens do tipo item por empresa
  let query = `
								SELECT 
									tii.*, ite.* 
								FROM ite_item ite 
								LEFT JOIN itk_item_kit itk 
									ON ite.ite_item_id = itk.itk_ite_item_id_kit 
								LEFT JOIN tii_tipo_item tii 
									ON ite.ite_tii_tipo_item_id = tii.tii_tipo_item_id AND tii.tii_tipo = 'I' 
								WHERE ite.ite_status = 1 AND itk.itk_ite_item_id_kit IS NULL 
									AND ite.ite_uni_unidade_id = ? AND tii.tii_tipo_item_id = ?
							`
  let values = [param.id, param.type]

  pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, results)
    })
  })
}

function post(data, callback) {
  // Inserir tipo item 
  pool.getConnection((error, conn) => {
    if (error)
      return callback(error)

    conn.beginTransaction(err => {
      if (err) {
        conn.rollback()
        return callback(err)
      }

      let sql = 'INSERT INTO tii_tipo_item SET ?'
      let params = {
        tii_descricao: data.tii_descricao,
        cat_categoria_id: data.cat_categoria_id,
        tii_valor: data.tii_valor,
        tii_valor_emergencial: data.tii_valor_emergencial,
        tii_medidas: data.tii_medidas,
        tii_peso_liquido: data.tii_peso_liquido,
        tii_peso_bruto: data.tii_peso_bruto,
        tii_tipo: data.tii_tipo,
        tii_arquivo: data.tii_arquivo,
        tii_emp_empresa_id: data.tii_emp_empresa_id,
        tii_created_by: data.tii_created_by
      }

      conn.query(sql, params, (error, results, fields) => {
        if (error) {
          conn.rollback()
          return callback(error)
        }

        const itemType = results.insertId

        const saveGallery = new Promise((resolve, reject) => {
          const itemTypePhotos = data.files['gallery']

          if (!itemTypePhotos)
            resolve()

          itemTypePhotos.forEach((photo, index) => {
            photo.path = photo.path.replace(/\\/g, '/')

            let sql = 'INSERT INTO mid_midia SET ?'
            let params = {
              mid_tii_tipo_item_id: itemType,
              mid_href: photo.path,
              mid_tipo: 'FOTO',
              mid_created_by: data.tii_created_by
            }

            conn.query(sql, params, (error, results, fields) => {
              if (error) reject(error)
              if (index === itemTypePhotos.length - 1) resolve()
            })
          })
        })

        if (!data.files['image']) {
          saveGallery
            .then(() => {
              conn.commit(error => {
                if (error) {
                  conn.rollback()
                  return callback(error)
                }
                return callback(null, { id: itemType })
              })
            })
            .catch(error => {
              conn.rollback()
              return callback(error)
            })
        } else {
          const itemTypePhotoProfile = data.files['image'][0]
          itemTypePhotoProfile.path = itemTypePhotoProfile.path.replace(/\\/g, '/')

          sql = 'INSERT INTO mid_midia SET ?'
          params = {
            mid_tii_tipo_item_id: itemType,
            mid_href: itemTypePhotoProfile.path,
            mid_tipo: 'FOTO',
            mid_created_by: data.tii_created_by
          }

          conn.query(sql, params, (error, results, fields) => {
            if (error) {
              conn.rollback()
              return callback(error)
            }

            itemTypePhotoProfile.id = results.insertId

            sql = 'UPDATE tii_tipo_item SET tii_mid_midia_id_perfil = ? WHERE tii_tipo_item_id = ?'
            params = [itemTypePhotoProfile.id, itemType]

            conn.query(sql, params, (error, results, fields) => {
              if (error) {
                conn.rollback()
                return callback(error)
              }

              saveGallery
                .then(() => {
                  conn.commit(error => {
                    if (error) {
                      conn.rollback()
                      return callback(error)
                    }
                    return callback(null, { id: itemType })
                  })
                })
                .catch(error => {
                  conn.rollback()
                  return callback(error)
                })
            })
          })
        }
      })
    })
  })
}

function putId(param, data, callback) {
  // Atualizar informações da tipo item por ID
  let query = 'UPDATE tii_tipo_item SET ? WHERE ?'
  let values = [
    {
      tii_descricao: data.tii_descricao,
      tii_valor: data.tii_valor,
      tii_valor_emergencial: data.tii_valor_emergencial,
      tii_medidas: data.tii_medidas,
      tii_peso_liquido: data.tii_peso_liquido,
      tii_peso_bruto: data.tii_peso_bruto,
      tii_tipo: data.tii_tipo,
      tii_arquivo: data.tii_arquivo,
      tii_emp_empresa_id: data.tii_emp_empresa_id,
      cat_categoria_id: data.cat_categoria_id
    },
    { tii_tipo_item_id: param.id }
  ]

  pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, { success: true })
    })
  })
}

function patchId(param, data, callback) {
  if (data.file)
    data.tii_arquivo = data.file.path.replace(/\\/g, '/')

  delete data.file

  let query = 'Update tii_tipo_item SET ? WHERE ?'
  let values = [data, { tii_tipo_item_id: param.id }]

  pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, { success: true })
    })
  })
}

function deleteId(param, callback) {
  // deletar o tipo item
  let query = 'DELETE from tii_tipo_item WHERE ?'
  let values = [{ tii_tipo_item_id: param.id }]

  pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, { success: true })
    })
  })
}

function getMinimumItemTypesFromUnities(itemTypeId, unityId, params, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({ message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500, error })

    const sql = `
									SELECT 
										tii.tii_descricao, tiu.tiu_quant_min
									FROM tiu_tipo_item_unidade tiu 
									JOIN tii_tipo_item tii 
										ON tiu.tiu_tii_tipo_item_id = tii.tii_tipo_item_id 
									JOIN uni_unidade uni 
										ON tiu.tiu_uni_unidade_id = uni.uni_unidade_id 
									WHERE tiu.tiu_uni_unidade_id = ?
										AND tii.tii_tipo_item_id = ?
								`
    const values = [unityId, itemTypeId]
    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) return callback({ status: 400, error })
      return callback(null, results)
    })
  })
}

function updateMinimumItemTypesFromUnity(itemTypeId, unityId, data, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({ message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500, error })

    const sql = `
									UPDATE tiu_tipo_item_unidade tiu 
									SET tiu.tiu_quant_min = ?
									WHERE tiu.tiu_tii_tipo_item_id = ? 
										AND tiu.tiu_uni_unidade_id = ?
								`
    const values = [data.tiu_quant_min, itemTypeId, unityId]
    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) return callback({ status: 400, error })
      return callback(null, null)
    })
  })
}

module.exports = {
  get,
  getId,
  getTypes,
  getCategoryTypes,
  getCompanyTypes,
  getCompanyTypesItens,
  getMinimumItemTypesFromUnities,
  post,
  putId,
  updateMinimumItemTypesFromUnity,
  patchId,
  deleteId
}