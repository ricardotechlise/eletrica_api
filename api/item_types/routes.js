const express = require("express")
const item_typesController = require("./item_typesController")
const verifyToken = require('../middlewares/verify')
const upload = require('../middlewares/upload')

function init() {	  
	let routes = express.Router()

	routes.route('/')
		.get(item_typesController.get)
		.post(verifyToken, upload.fields([{
				name: 'image',
				maxCount: 1
			}, {
				name: 'gallery',
				maxCount: 5
		}]), item_typesController.post)
		
	routes.route('/types')
		.get(verifyToken, item_typesController.getTypes)

	routes.route('/category')
		.get(item_typesController.getCategoryTypes)
	
	routes.route('/types/companytypes/:id')
		.get(verifyToken, item_typesController.getCompanyTypes)
	
	routes.route('/types/company/:id/types/:type')
		.get(verifyToken, item_typesController.getCompanyTypesItens)

	routes.route('/:itemTypeId/unities/:unityId')
		.get(verifyToken, item_typesController.getMinimumItemTypesFromUnities)
		.put(verifyToken, item_typesController.updateMinimumItemTypesFromUnity)

  	routes.route('/:id')
		.get(verifyToken, item_typesController.getId)
		.put(verifyToken, item_typesController.putId)
		.patch(verifyToken, upload.single('datasheet'), item_typesController.patchId)
		.delete(verifyToken, item_typesController.deleteId)

	return routes
}

module.exports = {init}
