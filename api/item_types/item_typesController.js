const item_types = require('./item_types.js')

function get (req, res, next) {
  item_types.get(req.query, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getId (req, res, next) {
  item_types.getId(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getTypes (req, res, next) {
  item_types.getTypes(req.query, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getCategoryTypes(req, res, next) {
  item_types.getCategoryTypes((error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.json(data)
  })
}

function getCompanyTypes (req, res, next) {
  item_types.getCompanyTypes(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getCompanyTypesItens (req, res, next) {
  item_types.getCompanyTypesItens(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function post (req, res, next) {
  req.body.files = req.files
  item_types.post (req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function putId (req, res, next) {
  item_types.putId(req.params, req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function patchId (req, res, next) {
  req.body.file = req.file

  item_types.patchId(req.params, req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function deleteId(req, res, next){
  item_types.deleteId(req.params, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function getMinimumItemTypesFromUnities(req, res) {
  item_types.getMinimumItemTypesFromUnities(req.params.itemTypeId, req.params.unityId, req.query, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.json(data)
  })
}

function updateMinimumItemTypesFromUnity(req, res) {
  item_types.updateMinimumItemTypesFromUnity(req.params.itemTypeId, req.params.unityId, req.body, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.status(204).json(data)
  })
}

module.exports = {
  get,
  getId,
  getTypes,
  getCategoryTypes,
  getCompanyTypes,
  getCompanyTypesItens,
  getMinimumItemTypesFromUnities,
  post,
  putId,
  updateMinimumItemTypesFromUnity,
  patchId,
  deleteId
}
