const express = require('express')
const logsController = require('./logsController')
const verifyToken = require('../middlewares/verify')

function init() {
  const routes = express.Router()

  routes.route('/')
    .post(verifyToken, logsController.createLog)
    .get(verifyToken, logsController.getLogs)

  return routes
}

module.exports = { init }
