const logs = require('./logs')

function createLog(req, res) {
  logs.createLog(req.body, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.json(data)
  })
}

function getLogs(req, res) {
  logs.getLogs(req.query, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.json(data)
  })
}

module.exports = {
  createLog,
  getLogs
}
