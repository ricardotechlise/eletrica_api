const mysql = require('mysql')
const config = require('../../config')

const pool = mysql.createPool(config.mysqlConfig)

function createLog(data, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500, error})
  
    const sql = `INSERT INTO log_logs SET ? `
    const values = {log_usr_user_id: data.log_usr_user_id}

    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) return callback({status: 400, error})
      return callback(null, {id: results.insertId})
    })
  })
}

function getLogs(params, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500, error})
    
    const sql = `
                  SELECT 
                    pes.pes_nome AS 'nome', usr.usr_apelido AS 'usuario', DATE_FORMAT(log.log_acesso,'%d/%m/%Y %H:%i:%s') AS 'data_acesso'
                  FROM log_logs log
                  LEFT JOIN usr_user usr 
                    ON log.log_usr_user_id = usr.usr_user_id
                  LEFT JOIN pes_pessoa pes 
                    ON usr.usr_user_id = pes.pes_usr_user_id_acesso
                `
    conn.query(sql, (error, results, fields) => {
      conn.release()
      if (error) return callback({status: 400, error})
      return callback(null, results)
    })
  })
}

module.exports = {
  createLog,
  getLogs
}
