const express = require("express")
const kitController = require("./kitController")
const verifyToken = require('../middlewares/verify')

function init() {

    let routes = express.Router()

    routes.route('/')
        .post(verifyToken, kitController.post)
        .put(verifyToken, kitController.putId)
        .delete(verifyToken, kitController.deleteId)

    routes.route('/companykits/:id/:item')
        .get(verifyToken, kitController.getCompanyKits)
    
    routes.route('/companykits/:kit/items/:item')
        .get(verifyToken, kitController.getCompanyItemKits)
        
    routes.route('/:id/')
        .get(verifyToken, kitController.getId)

    return routes

}

module.exports = {
    init
}