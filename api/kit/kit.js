const mysql = require('mysql')
const config = require('../../config')

var pool = mysql.createPool(config.mysqlConfig)

function post(data, callback) {

    // Inserir item ao kit

    let itk_ite_item_id_kit = data.itk_ite_item_id_kit
    let itk_ite_item_id_item = data.itk_ite_item_id_item
    let itk_created_by = data.itk_created_by

    let query = 'INSERT INTO itk_item_kit SET ?'
    let values = [{ itk_ite_item_id_kit: itk_ite_item_id_kit, itk_ite_item_id_item: itk_ite_item_id_item, itk_created_by: itk_created_by }]

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            if (error) {
                connection.release()
                return callback(error)
            } else {
                return callback(null, { success: results })
            }
        })
    })
}

function putId(data, data, callback) {

    // Ativa item por kit

    let itk_ite_item_id_kit = data.itk_ite_item_id_kit
    let itk_ite_item_id_item = data.itk_ite_item_id_item
    let itk_status = 1

    let query = 'UPDATE itk_item_kit SET ? WHERE ?'
    let values = [{ itk_status: itk_status }, { itk_ite_item_id_kit: itk_ite_item_id_kit, itk_ite_item_id_item: itk_ite_item_id_item }]

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) return callback(error)
            return callback(null, { success: true })
        })
    })
}

function deleteId(data, callback) {

    // Desativar o item por kit

    let itk_ite_item_id_kit = data.itk_ite_item_id_kit
    let itk_ite_item_id_item = data.itk_ite_item_id_item
    let itk_status = 0

    let values = [{ itk_status: itk_status }, { itk_ite_item_id_kit: itk_ite_item_id_kit, itk_ite_item_id_item: itk_ite_item_id_item }]

    let query = 'UPDATE itk_item_kit SET ? WHERE ?'

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) return callback(error)
            return callback(null, { success: true })
        })
    })
}

function getId(route, param, callback) {

    // Buscar informações do kit

    let itk_ite_item_id_item = data.itk_ite_item_id_item

    let query = ''
    let values = ''

    switch (route) {
        case '1':
            // Busca ites por id

            let itk_ite_item_id_kit = data.itk_ite_item_id_kit

            values = [itk_ite_item_id_kit]

            query = 'WHERE itk_ite_item_id_kit = ?'

            break;

        case '2':
            // Busca itens de uma determinada unidade

            ite_uni_unidade_id = param.ite_uni_unidade_id

            values = [ite_uni_unidade_id]

            query = 'SELECT tii.tii_descricao, tii.tii_atributo, tii.tii_tipo, tii.tii_arquivo, ite.ite_item_id, ite.ite_uni_unidade_id, uni.uni_nome_fantasia, ite.ite_tii_tipo_item_id, ite.ite_status, ite.ite_qrcode, ite.ite_created_by, ite.ite_created_at FROM ite_item ite JOIN tii_tipo_item tii ON ite.ite_tii_tipo_item_id = tii.tii_tipo_item_id JOIN uni_unidade uni ON ite.ite_uni_unidade_id = uni.uni_unidade_id WHERE ite.ite_uni_unidade_id = ?'

            break;

        case '3':
            // Busca itens ativos de uma determinada unidade

            ite_uni_unidade_id = param.ite_uni_unidade_id

            values = [ite_uni_unidade_id]

            query = 'SELECT tii.tii_descricao, tii.tii_atributo, tii.tii_tipo, tii.tii_arquivo, ite.ite_item_id, ite.ite_uni_unidade_id, uni.uni_nome_fantasia, ite.ite_tii_tipo_item_id, ite.ite_status, ite.ite_qrcode, ite.ite_created_by, ite.ite_created_at FROM ite_item ite JOIN tii_tipo_item tii ON ite.ite_tii_tipo_item_id = tii.tii_tipo_item_id JOIN uni_unidade uni ON ite.ite_uni_unidade_id = uni.uni_unidade_id WHERE ite.ite_uni_unidade_id = ? AND ite.ite_status = 1'

            break;

        case '4':
            // Busca item pelo qrcode

            let ite_qrcode = param.ite_qrcode

            values = [ite_qrcode]

            query = 'SELECT tii.tii_descricao, tii.tii_atributo, tii.tii_tipo, tii.tii_arquivo, ite.ite_item_id, ite.ite_uni_unidade_id, uni.uni_nome_fantasia, ite.ite_tii_tipo_item_id, ite.ite_status, ite.ite_qrcode, ite.ite_created_by, ite.ite_created_at FROM ite_item ite JOIN tii_tipo_item tii ON ite.ite_tii_tipo_item_id = tii.tii_tipo_item_id JOIN uni_unidade uni ON ite.ite_uni_unidade_id = uni.uni_unidade_id WHERE ite.ite_qrcode = ?'

            break;
    }

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) return callback(error)
            var data = JSON.stringify(results)
            return callback(null, JSON.parse(data))
        })
    })
}

function getCompanyKits (param, callback) {
	// Buscar informações do tipo item
	let query = `SELECT tii.*, ite.* FROM ite_item ite JOIN tii_tipo_item tii ON ite.ite_tii_tipo_item_id = tii.tii_tipo_item_id AND tii.tii_tipo = 'K' WHERE ite.ite_uni_unidade_id = ? AND ite.ite_status = 1 AND ite.ite_tii_tipo_item_id = ?`
	let values = [param.id, param.item]

	pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, results)
    })
  })
}

function getCompanyItemKits (param, callback) {
	// Buscar informações do tipo item
	let query = `SELECT tii.*, ite.* FROM itk_item_kit itk JOIN ite_item ite ON itk.itk_ite_item_id_item = ite.ite_item_id JOIN tii_tipo_item tii ON ite.ite_tii_tipo_item_id = tii.tii_tipo_item_id AND tii.tii_tipo = 'I' WHERE itk.itk_ite_item_id_kit = ? AND tii.tii_tipo_item_id = ? AND ite.ite_status = 1 AND itk.itk_status = 1`
	let values = [param.kit, param.item]

	pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, results)
    })
  })
}

module.exports = {
    post,
    putId,
    deleteId,
    getId,
    getCompanyKits,
    getCompanyItemKits
}