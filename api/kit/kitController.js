const kit   = require('./kit')

function post (req, res, next) {
  kit.post (req.body, (err, data) => {
    if (err) {
      res.status(500).send(err)
    } else {
      // console.log(res)
      res.json(data)
    }
  })
}

function putId (req, res, next) {
  kit.putId(req.body, (err, data) => {
      if (err) res.status(500).send(err)
      else {
        res.json(data)
      }
  })
}

function deleteId(req, res, next){
  kit.deleteId(req.body, (err, data) => {
    if (err) res.status(500).send(err)
    else {
      res.json(data)
    }
  })
}

function getId (req, res, next) {
  kit.getId(req.params.id, req.query, (err, content) => {
    if (err) {
      res.status(500).send(err)
    } else {
      // console.log(res)
      res.json(content)
    }
  })
}

function getCompanyKits (req, res, next) {
  kit.getCompanyKits(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getCompanyItemKits (req, res, next) {
  kit.getCompanyItemKits(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

module.exports = {
  post,
  putId,
  deleteId,
  getId,
  getCompanyKits,
  getCompanyItemKits
}
