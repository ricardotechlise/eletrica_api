const mysql = require('mysql')
const config = require('../../config')

const pool = mysql.createPool(config.mysqlConfig)

function getReplacement(params, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500, error})
    
    let sql = `
                SELECT  uni.uni_razao_social AS 'Unidade', 
                  tii.tii_descricao AS 'Produto', 
                  tiu.tiu_quant AS 'Quantidade_atual', 
                  tiu_quant_min AS 'Quantidade_minimo', 
                    CASE WHEN (tiu_quant_min - tiu.tiu_quant) < 0 THEN 0 
                    ELSE tiu_quant_min - tiu.tiu_quant 
                    END AS 'Reposicao' 
                FROM tiu_tipo_item_unidade tiu 
                JOIN tii_tipo_item tii 
                  ON tiu.tiu_tii_tipo_item_id = tii.tii_tipo_item_id 
                JOIN uni_unidade uni 
                  ON tiu.tiu_uni_unidade_id = uni.uni_unidade_id
              `
    let values = []

    if (params.tiu_uni_unidade_id) {
      sql += `
                WHERE tiu.tiu_uni_unidade_id = ?
             `
      values = [...values, params.tiu_uni_unidade_id]
    }
    
    conn.query(sql, values, (error, results, fields) => {
      if (error) return callback({error, status: 400})
      return callback(null, results)
    })
    
  })
}

function getProductOutput(params, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500, error})
  
    let sql = `
                SELECT uni.uni_razao_social, ite.ite_item_id, 
                  tii.tii_descricao, ite.ite_retirada, ite_recebido 
                FROM ite_item ite
                JOIN tii_tipo_item tii 
                  ON ite.ite_tii_tipo_item_id = tii.tii_tipo_item_id
                JOIN uni_unidade uni 
                  ON ite.ite_uni_unidade_id = uni.uni_unidade_id
              `
    let values = []

    if (params.uni_unidade_id) {
      sql += `
              WHERE uni.uni_unidade_id = ?
             `
      values = [...values, params.uni_unidade_id]
    }

    if (params.initialDate && params.finalDate) {
      sql += 
              values.length > 0
                ? ` 
                    AND ite.ite_retirada BETWEEN ? AND ?
                  `
                : ` 
                    WHERE ite.ite_retirada BETWEEN ? AND ? 
                  `
      values = [...values, params.initialDate, params.finalDate]
    }
    
    conn.query(sql, values, (error, results, fields) => {
      if (error) return callback({error, status: 400})
      return callback(null, results)
    })
  })
}

function getTotalSales(params, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500, error})
    
    let sql = `
                SELECT tii.tii_descricao, COUNT(ite.ite_retirada) AS 'Venda'
                FROM tii_tipo_item tii 
                LEFT JOIN ite_item ite 
                  ON tii.tii_tipo_item_id = ite.ite_tii_tipo_item_id
              `
    let values = []

    if (params.initialDate && params.finalDate) {
      sql += `
              WHERE ite.ite_retirada BETWEEN ? AND ?
             `
      values = [...values, params.initialDate, params.finalDate]
    }

    if (params.tii_tipo) {
      sql += 
             values.length > 0
              ? `
                  AND tii.tii_tipo = ?
                `
              : `
                  WHERE tii.tii_tipo = ?
                `
      values = [...values, params.tii_tipo]
    }

    sql += `
            GROUP BY 1 ORDER BY 2 DESC, 1 ASC
           `

    conn.query(sql, values, (error, results, fields) => {
      if (error) return callback({error, status: 400})
      return callback(null, results)
    })
  })
}

function getSaleAverageTime(params, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500, error})

    let sql = `
                select uni.uni_razao_social, tii.tii_descricao, 
                        round(sum(c.numdias)/
                          (case when count(c.ite_tii_tipo_item_id)-1 > 0 
                            then count(c.ite_tii_tipo_item_id)-1 
                            else 1 end),0) as media
                from  (select 	a.ite_uni_unidade_id, a.ite_tii_tipo_item_id, DATE_FORMAT(a.ite_retirada,'%d/%m/%Y') AS 'ite_retirada', 
                        (case when (select coalesce(datediff(b.ite_retirada,a.ite_retirada)) from ite_item b  where b.ite_uni_unidade_id = a.ite_uni_unidade_id and b.ite_tii_tipo_item_id = a.ite_tii_tipo_item_id and 
                        b.ite_retirada > a.ite_retirada order by b.ite_retirada LIMIT 1 ) > 0 then (select coalesce(datediff(b.ite_retirada,a.ite_retirada)) from ite_item b  where b.ite_uni_unidade_id = a.ite_uni_unidade_id and b.ite_tii_tipo_item_id = a.ite_tii_tipo_item_id and 
                        b.ite_retirada > a.ite_retirada order by b.ite_retirada LIMIT 1 ) else datediff(NOW(),a.ite_retirada) end) as numdias
                    from ite_item a WHERE a.ite_retirada IS NOT NULL group by 1,2,3,4) c
                JOIN tii_tipo_item tii 
                  ON c.ite_tii_tipo_item_id = tii.tii_tipo_item_id 
                JOIN uni_unidade uni 
                  ON c.ite_uni_unidade_id = uni.uni_unidade_id 
              `
    let values = []

    if (params.uni_unidade_id) {
      sql += `
              WHERE uni.uni_unidade_id = ?
             `
      values = [...values, params.uni_unidade_id]
    }

    sql += `
            GROUP BY 1, 2
           `

    conn.query(sql, values, (error, results, fields) => {
      if (error) return callback({error, status: 400})
      return callback(null, results)
    })
  })
}

function getAccessLogs(params, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({message: 'Não foi possível estabelecer uma conexão com o banco de dados.', status: 500, error})

    let sql = `
                SELECT pes.pes_nome AS 'nome', usr.usr_apelido AS 'usuario', DATE_FORMAT(log.log_acesso,'%d/%m/%Y %H:%i:%s') AS 'data_acesso'
                FROM log_logs log
                LEFT JOIN usr_user usr 
                  ON log.log_usr_user_id = usr.usr_user_id
                LEFT JOIN pes_pessoa pes 
                  ON usr.usr_user_id = pes.pes_usr_user_id_acesso
              `
    let values = []

    if (params.pes_pessoa_id) {
      sql += `
              WHERE pes.pes_pessoa_id = ?
             `
      values = [...values, params.pes_pessoa_id]
    }

    conn.query(sql, values, (error, results, fields) => {
      if (error) return callback({error, status: 400})
      return callback(null, results)
    })
  })
}

module.exports = {
  getReplacement,
  getProductOutput,
  getTotalSales,
  getSaleAverageTime,
  getAccessLogs
}
