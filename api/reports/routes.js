const express = require('express')
const reportsController = require('./reportsController')
const verifyToken = require('../middlewares/verify')

function init() {
  const routes = express.Router()

  routes.route('/replacement').get(verifyToken, reportsController.getReplacement)

  routes.route('/product-output').get(verifyToken, reportsController.getProductOutput)

  routes.route('/total-sales').get(verifyToken, reportsController.getTotalSales)

  routes.route('/sale-average-time').get(verifyToken, reportsController.getSaleAverageTime)

  routes.route('/access').get(verifyToken, reportsController.getAccessLogs)

  return routes
}

module.exports = { init }
