const reports = require('./reports')

function getReplacement(req, res) {
  reports.getReplacement(req.query, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.json(data)
  })
}

function getProductOutput(req, res) {
  reports.getProductOutput(req.query, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.json(data)
  })
}

function getTotalSales(req, res) {
  reports.getTotalSales(req.query, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.json(data)
  })
}

function getSaleAverageTime(req, res) {
  reports.getSaleAverageTime(req.query, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.json(data)
  })
}

function getAccessLogs(req, res) {
  reports.getAccessLogs(req.query, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.json(data)
  })
}

module.exports = {
  getReplacement,
  getProductOutput,
  getTotalSales,
  getSaleAverageTime,
  getAccessLogs
}
