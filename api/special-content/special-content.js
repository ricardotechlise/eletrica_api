const mysql = require('mysql')
const fs = require('fs')
const config = require('../../config')

const pool = mysql.createPool(config.mysqlConfig)

function getSpecialContent(params, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({error, message: 'Falha ao estabelecer conexão com o banco de dados.', status: 500})

    let sql = `
                SELECT ces.* FROM ces_conteudo_especial ces
              `
    let values = []

    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) return callback({error, status: 400})
      return callback(null, results)
    })
  })
}

function getActiveSpecialContent(params, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({error, message: 'Falha ao estabelecer conexão com o banco de dados.', status: 500})

    let sql = `
                SELECT ces.* FROM ces_conteudo_especial ces where ces_status = 1
              `
    let values = []

    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) return callback({error, status: 400})
      return callback(null, results)
    })
  })
}

function createSpecialContent(data, callback) {
  const deleteUploads = () => {
    if (data.files['thumbnail'])
      fs.unlink('./' + data.files['thumbnail'][0].path.replace(/\\/g, '/'))

    if (data.files['file'])
      fs.unlink('./' + data.files['file'][0].path.replace(/\\/g, '/'))
  }

  if (!data.files['thumbnail'] || (!data.files['file'] && !data.ces_arquivo)) {
    deleteUploads()
    return callback({message: 'Ausência de thumbnail e/ou arquivo/url.', status: 400})
  }

  data.ces_thumbnail = data.files['thumbnail'][0].path.replace(/\\/g, '/')
  data.ces_arquivo = data.files['file'] ? data.files['file'][0].path.replace(/\\/g, '/') : data.ces_arquivo

  pool.getConnection((error, conn) => {
    if (error) {
      deleteUploads()
      return callback({error, message: 'Falha ao estabelecer conexão com o banco de dados.', status: 500})
    }

    const sql = `
                  INSERT INTO ces_conteudo_especial SET ?
                `
    const values = {
      ces_titulo: data.ces_titulo,
      ces_thumbnail: data.ces_thumbnail,
      ces_arquivo: data.ces_arquivo,
      ces_arquivo_tipo: data.ces_arquivo_tipo,
      ces_created_by: data.ces_created_by
    }

    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) {
        deleteUploads()
        return callback({error, status: 400})
      }
      return callback(null, {id: results.insertId})
    })
  })
}

function updateSpecialContent(id, data, callback) {
  const deleteUploads = () => {
    if (data.files['thumbnail'])
      fs.unlink('./' + data.files['thumbnail'][0].path.replace(/\\/g, '/'))

    if (data.files['file'])
      fs.unlink('./' + data.files['file'][0].path.replace(/\\/g, '/'))
  }

  pool.getConnection((error, conn) => {
    if (error) {
      deleteUploads()
      return callback({error, message: 'Falha ao estabelecer conexão com o banco de dados.', status: 500})
    }

    const sql = `
                  UPDATE ces_conteudo_especial SET ? WHERE ces_conteudo_especial_id = ?
                `
    let values = [{ces_titulo: data.ces_titulo, ces_arquivo_tipo: data.ces_arquivo_tipo}, id]

    if (data.files['thumbnail'])
      values[0].ces_thumbnail = data.files['thumbnail'][0].path.replace(/\\/g, '/')

    if (data.files['file'])
      values[0].ces_arquivo = data.files['file'][0].path.replace(/\\/g, '/')

    if (!data.files['file'] && data.ces_arquivo)
      values[0].ces_arquivo = data.ces_arquivo

    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) {
        deleteUploads()
        return callback({error, status: 400})
      }
      return callback(null, null)
    })
  })
}

function changeSpecialContent(id, data, callback) {
  pool.getConnection((error, conn) => {
    if (error)
      return callback({error, message: 'Falha ao estabelecer conexão com o banco de dados.', status: 500})

    const sql = `
                  UPDATE ces_conteudo_especial SET ? WHERE ces_conteudo_especial_id = ?
                `
    const values = [{
      ces_status: data.ces_status
    }, id]

    conn.query(sql, values, (error, results, fields) => {
      conn.release()
      if (error) return callback({error, status: 400})
      return callback(null, null)
    })
  })
}

module.exports = {
  getSpecialContent,
  getActiveSpecialContent,
  createSpecialContent,
  updateSpecialContent,
  changeSpecialContent
}
