const express = require('express')
const specialContentController = require('./special-contentController')
const verifyToken = require('../middlewares/verify')
const upload = require('../middlewares/upload')

function init() {
  const routes = express.Router()
  
  routes.route('/')
    .get(verifyToken, specialContentController.getSpecialContent)
    .post(verifyToken, upload.fields([
      {name: 'thumbnail', maxCount: 1}, 
      {name: 'file', maxCount: 1}
    ]), specialContentController.createSpecialContent)
  
  routes.route('/active')
    .get(specialContentController.getActiveSpecialContent)

  routes.route('/:id')
    .put(verifyToken, upload.fields([
      {name: 'thumbnail', maxCount: 1}, 
      {name: 'file', maxCount: 1}
    ]), specialContentController.updateSpecialContent)
    .patch(verifyToken, specialContentController.changeSpecialContent)

  return routes
}

module.exports = { init }
