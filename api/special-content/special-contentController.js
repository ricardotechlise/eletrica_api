const specialContent = require('./special-content')

function getSpecialContent(req, res) {
  specialContent.getSpecialContent(req.query, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.json(data)
  })
}

function getActiveSpecialContent(req, res) {
  specialContent.getActiveSpecialContent(req.query, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.json(data)
  })
}

function createSpecialContent(req, res) {
  req.body.files = req.files

  specialContent.createSpecialContent(req.body, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.status(201).json(data)
  })
}

function updateSpecialContent(req, res) {
  req.body.files = req.files

  specialContent.updateSpecialContent(req.params.id, req.body, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.status(204).json(data)
  })
}

function changeSpecialContent(req, res) {
  specialContent.changeSpecialContent(req.params.id, req.body, (error, data) => {
    if (error) return res.status(error.status).json(error)
    return res.status(204).json(data)
  })
}

module.exports = {
  getSpecialContent,
  getActiveSpecialContent,
  createSpecialContent,
  updateSpecialContent,
  changeSpecialContent
}
