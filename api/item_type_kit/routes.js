const express = require("express")
const item_type_kitController = require("./item_type_kitController")
const verifyToken = require('../middlewares/verify')

function init() {
	let routes = express.Router()

	routes.route('/')
		.get(verifyToken, item_type_kitController.get)
		.post(verifyToken, item_type_kitController.post)

	routes.route('/companykits/:id')
		.get(verifyToken, item_type_kitController.getCompanyKits)
	
	routes.route('/companykits/items/:id')
		.get(verifyToken, item_type_kitController.getCompanyKitItems)
		
	routes.route('/kit/:id')
		.get(verifyToken, item_type_kitController.getKitItem)

  	routes.route('/:id/')
		.get(verifyToken, item_type_kitController.getId)
		.put(verifyToken, item_type_kitController.putId)
		.patch(verifyToken, item_type_kitController.patchId)
		.delete(verifyToken, item_type_kitController.deleteId)

	return routes
}

module.exports = {init}