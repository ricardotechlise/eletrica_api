const mysql = require('mysql')
const config = require('../../config')
var pool = mysql.createPool(config.mysqlConfig)

function get (callback) {
	// Listar todos os itens de kit
  let query = 'SELECT tik_tipo_item_id_kit, tik_tipo_item_item, tik_quant, tik_status, tik_created_by, tik_created_at FROM tik_tipo_item_kit'
  
  pool.getConnection((err, connection) => {
    connection.query(query, (error, results, fields) => {
			connection.release()
    	if (error) return callback(error)
      return callback(null, results)
    })
  })
}
  
function getId (param, callback) {
	// Buscar informações do tipo item
	let query = 'SELECT tik_tipo_item_id_kit, tik_tipo_item_item, tik_quant, tik_status, tik_created_by, tik_created_at FROM tik_tipo_item_kit WHERE ?'
  let values = [{tik_tipo_item_id_kit: param.id}]

	pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, results)
    })
  })
}

function getCompanyKits (param, callback) {
	// Buscar informações do tipo Kit
	let query = `
								SELECT 
									tii.tii_tipo_item_id, tii.tii_descricao, count(*) as tii_quant
								FROM ite_item ite	
								JOIN tii_tipo_item tii ON ite.ite_tii_tipo_item_id = tii.tii_tipo_item_id AND	tii.tii_tipo = 'K' 
								WHERE ite.? AND ite.ite_status = 1 
								GROUP BY 1
							`
	let values = [{ite_uni_unidade_id: param.id}]

	pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
			if (error) return callback(error)
			const getMidia = new Promise((resolve, reject) => {
				let typeItens = results
				if (typeItens.length == 0) resolve(typeItens)
				typeItens.forEach((typeItem, index) => {
					query = `
										SELECT 
											mid.mid_midia_id, mid.mid_href, mid.mid_tipo,
											tii.tii_mid_midia_id_perfil
										FROM mid_midia mid 
										LEFT JOIN tii_tipo_item tii
											ON mid.mid_tii_tipo_item_id = tii.tii_tipo_item_id 
										WHERE mid_tii_tipo_item_id = ?
									`
					values = typeItem.tii_tipo_item_id
					connection.query(query, values, (error, results, fields) => {
						if (error)
							return reject(error)
						typeItens[index].mid_midia = results
						let midias = []
						let auxMidias = []
						typeItens[index].mid_midia = typeItens[index].mid_midia.filter((midia, index) => {
							midias = [...auxMidias]
							auxMidias = [...midias, midia.mid_midia_id]
							return midias.indexOf(midia.mid_midia_id) == -1
						})
						typeItens[index].perfil = typeItens[index].mid_midia.filter(midia => midia.tii_mid_midia_id_perfil == midia.mid_midia_id)[0] || {}
						typeItens[index].galeria = typeItens[index].mid_midia.filter(midia => midia.tii_mid_midia_id_perfil != midia.mid_midia_id)
						delete typeItens[index].mid_midia
						if (index == typeItens.length - 1)
							resolve(typeItens)
					})
				})
			})
			getMidia
			.then(typeItens => callback(null, typeItens))
			.catch(error => callback(error))
    })
  })
}

function getCompanyKitItems (param, callback) {
	// Retorna itens de um kit pertencentes a uma empresa
	let query = `
								SELECT 
									tii.tii_tipo_item_id, tii.tii_descricao, count(*) as tii_quant,
									tii.tii_mid_midia_id_perfil
								FROM itk_item_kit itk 
								JOIN ite_item ite 
									ON itk.itk_ite_item_id_item = ite.ite_item_id 
								JOIN tii_tipo_item tii 
									ON ite.ite_tii_tipo_item_id = tii.tii_tipo_item_id AND tii.tii_tipo = 'I'
								WHERE itk.itk_ite_item_id_kit = ? AND ite.ite_status = 1 AND itk.itk_status = 1 GROUP BY 1;`
	let values = [param.id]
	
	pool.getConnection((err, connection) => {
		if (err) return callback(err)
		connection.query(query, values, (error, results, fields) => {
			connection.release()
			if (error) return callback(error)
			const getMidia = new Promise((resolve, reject) => {
				let typeItens = results
				if (typeItens.length == 0) resolve(typeItens)
				typeItens.forEach((typeItem, index) => {
					query = `
										SELECT 
											mid.mid_midia_id, mid.mid_href, mid.mid_tipo,
											tii.tii_mid_midia_id_perfil
										FROM mid_midia mid 
										LEFT JOIN tii_tipo_item tii
											ON mid.mid_tii_tipo_item_id = tii.tii_tipo_item_id 
										WHERE mid_tii_tipo_item_id = ?
									`
					values = typeItem.tii_tipo_item_id
					connection.query(query, values, (error, results, fields) => {
						if (error)
							return reject(error)
						typeItens[index].mid_midia = results
						let midias = []
						let auxMidias = []
						typeItens[index].mid_midia = typeItens[index].mid_midia.filter((midia, index) => {
							midias = [...auxMidias]
							auxMidias = [...midias, midia.mid_midia_id]
							return midias.indexOf(midia.mid_midia_id) == -1
						})
						typeItens[index].perfil = typeItens[index].mid_midia.filter(midia => midia.tii_mid_midia_id_perfil == midia.mid_midia_id)[0] || {}
						typeItens[index].galeria = typeItens[index].mid_midia.filter(midia => midia.tii_mid_midia_id_perfil != midia.mid_midia_id)
						delete typeItens[index].mid_midia
						if (index == typeItens.length - 1)
							resolve(typeItens)
					})
				})
			})
			getMidia
			.then(typeItens => callback(null, typeItens))
			.catch(error => callback(error))
		})
	})
}

function getKitItem (param, callback) {
	// Buscar informações do tipo item
	let query = 'SELECT tik_tipo_item_id_kit, tik_tipo_item_item, tii.tii_descricao, tik_quant, tik_status, tik_created_by, tik_created_at FROM tik_tipo_item_kit JOIN tii_tipo_item tii on tii.tii_tipo_item_id = tik_tipo_item_item	WHERE ?'
	let values = [{tik_tipo_item_id_kit: param.id}]

	pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) return callback(error)
      return callback(null, results)
    })
  })
}

function post(data, callback) {
  // Inserir tipo item
  let query = 'INSERT INTO tik_tipo_item_kit SET ?'
  let values = [
		{
			tik_tipo_item_id_kit: data.tik_tipo_item_id_kit,
			tik_tipo_item_item: data.tik_tipo_item_item, 
			tik_quant: data.tik_quant, 
			tik_status: data.tik_status,
			tik_created_by: data.tik_created_by
		}
	]
  
  pool.getConnection( (err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release() 
			if (error) {
				console.log(error)
				return callback(error)
			} else {
				return callback(null, {success: true})
			}
    })
  })
}

function putId(param, data, callback) {
	// Atualizar informações do tipo item por ID
	let query = 'UPDATE tik_tipo_item_kit SET ? WHERE ?'
  let values = [
		{
			tik_tipo_item_id_kit: data.tik_tipo_item_id_kit,
			tik_tipo_item_item: data.tik_tipo_item_item,
			tik_quant: data.tik_quant,
			tik_status: data.tik_status,
		},
		{tik_tipo_item_id_kit: param.id}
	]

  pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
  		connection.release()
      if (error) return callback(error)
      return callback(null, {success: true})
    })
  })
}

function patchId(param, data, callback) {
	let query = 'Update tik_tipo_item_kit SET ? WHERE ? AND ?'
	let values = [{tik_status: data.tik_status, tik_quant: data.tik_quant}, {tik_tipo_item_id_kit: param.id}, {tik_tipo_item_item: data.tik_tipo_item_item}]

	pool.getConnection((err, connection) => {
    connection.query(query, values, (error, results, fields) => {
  		connection.release()
      if (error) return callback(error)
      return callback(null, {success: true})
    })
  })
}

function deleteId(param, callback) {
  // deletar o tipo item
  let query = 'DELETE from tik_tipo_item_kit WHERE ?'
	let values = [{tik_tipo_item_id_kit: param.id}]
  
  pool.getConnection((err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release()
			if (error) return callback(error)
			return callback(null, {success: true})
		})
  })
}

module.exports = {
	get,
	getId,
	getCompanyKits,
	getCompanyKitItems,
	getKitItem,
  post,
	putId,
	patchId,
  deleteId  
}