const item_type_kit = require('./item_type_kit.js')

function get (req, res, next) {
  item_type_kit.get((err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getId (req, res, next) {
  item_type_kit.getId(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getCompanyKits (req, res, next) {
  item_type_kit.getCompanyKits(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getCompanyKitItems (req, res, next) {
  item_type_kit.getCompanyKitItems(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function getKitItem (req, res, next) {
  item_type_kit.getKitItem(req.params, (err, content) => {
    if (err) res.status(500).send(err)
    res.json(content)
  })
}

function post (req, res, next) {
  item_type_kit.post (req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function putId (req, res, next) {
  item_type_kit.putId(req.params, req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function patchId (req, res, next) {
  item_type_kit.patchId(req.params, req.body, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

function deleteId(req, res, next){
  item_type_kit.deleteId(req.params, (err, data) => {
    if (err) res.status(500).send(err)
    res.json(data)
  })
}

module.exports = {
  get,
  getId,
  getCompanyKits,
  getCompanyKitItems,
  getKitItem,
  post,
  putId,
  patchId,
  deleteId
}
