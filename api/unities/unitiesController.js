const unities = require('./unities')

function get(req, res, next) {
    unities.get(req.query, (err, content) => {
        if (err) res.status(500).send(err)
        res.json(content)
    })
}

function getCompanies(req, res, next) {
    unities.getCompanies((err, content) => {
        if (err) res.status(500).send(err)
        res.json(content)
    })
}


function getId(req, res, next) {
    unities.getId(req.params, (err, content) => {
        if (err) res.status(500).send(err)
        res.json(content)
    })
}

function getCompanyUnities(req, res, next) {
    unities.getCompanyUnities(req.params, (err, content) => {
        if (err) res.status(500).send(err)
        res.json(content)
    })
}

function post(req, res, next) {
    unities.post(req.body, (err, data) => {
        if (err) res.status(500).send(err)
        res.json(data)
    })
}

function putId(req, res, next) {
    unities.putId(req.params, req.body, (err, data) => {
        if (err) res.status(500).send(err)
        res.json(data)
    })
}

function deleteId(req, res, next) {
    unities.deleteId(req.params, (err, data) => {
        if (err) res.status(500).send(err)
        res.json(data)
    })
}

module.exports = {
    get,
    getCompanies,
    getId,
    getCompanyUnities,
    post,
    putId,
    deleteId
}