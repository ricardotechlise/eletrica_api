const mysql = require('mysql')
const config = require('../../config')
var pool = mysql.createPool(config.mysqlConfig)

function get(params, callback) {
    // Listar todas as unidade
    let query = 'SELECT uni_unidade_id, uni_razao_social, uni_nome_fantasia, uni_emp_empresa_id, uni_pes_pessoa_id_contato, uni_status, uni_tipo, uni_cnpj, uni_cep, uni_logradouro, uni_complemento, uni_numero, uni_bairro, uni_cidade, uni_estado, uni_pais, uni_email, uni_created_by, uni_created_at FROM uni_unidade'
    let values = []

    if (params.uni_emp_empresa_id) {
        query += ' WHERE uni_emp_empresa_id = ?'
        values = [...values, params.uni_emp_empresa_id]
    }

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) return callback(error)
            return callback(null, results)
        })
    })
}

function getCompanies(callback) {
    // Buscar unidades de uma empresa
    let query = `SELECT uni_unidade_id,
									uni.uni_razao_social,
									uni.uni_nome_fantasia,
									uni.uni_emp_empresa_id,
									uni.uni_pes_pessoa_id_contato,
									uni.uni_status,
									uni.uni_tipo,
									uni.uni_cnpj,
									uni.uni_cep,
									uni.uni_logradouro,
									uni.uni_complemento,
									uni.uni_numero,
									uni.uni_bairro,
									uni.uni_cidade,
									uni.uni_estado,
									uni.uni_pais,
									uni.uni_email,
									uni.uni_created_by,
									uni.uni_created_at,
									tel.tel_ddd,
									tel.tel_telefone
								FROM uni_unidade uni
									LEFT JOIN tel_telefone tel 
                                ON uni.uni_unidade_id = tel.tel_uni_unidade_id
                                WHERE uni.uni_emp_empresa_id = 1`
    pool.getConnection((err, connection) => {
        connection.query(query, (error, results, fields) => {
            connection.release()
            if (error) return callback(error)
            return callback(null, results)
        })
    })
}

function getId(param, callback) {
    // Buscar informações da unidade
    let query = 'SELECT uni_unidade_id, uni_razao_social, uni_nome_fantasia, uni_emp_empresa_id, uni_pes_pessoa_id_contato, uni_status, uni_tipo, uni_cnpj, uni_cep, uni_logradouro, uni_complemento, uni_numero, uni_bairro, uni_cidade, uni_estado, uni_pais, uni_email, uni_created_by, uni_created_at FROM uni_unidade WHERE ?'
    let values = [{ uni_unidade_id: param.id }]

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) return callback(error)
            return callback(null, results)
        })
    })
}

function getCompanyUnities(param, callback) {
    // Buscar unidades de uma empresa
    let query = 'SELECT uni_unidade_id, uni_razao_social, uni_nome_fantasia, uni_emp_empresa_id, uni_pes_pessoa_id_contato, uni_status, uni_tipo, uni_cnpj, uni_cep, uni_logradouro, uni_complemento, uni_numero, uni_bairro, uni_cidade, uni_estado, uni_pais, uni_email, uni_created_by, uni_created_at FROM uni_unidade WHERE ?'
    let values = [{ uni_emp_empresa_id: param.id }]
    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) return callback(error)
            return callback(null, results)
        })
    })
}

function post(data, callback) {
    // Inserir unidade
    let uni_razao_social = data.uni_razao_social
    let uni_nome_fantasia = data.uni_nome_fantasia
    let uni_emp_empresa_id = data.uni_emp_empresa_id
    let uni_tipo = data.uni_tipo
    let uni_cnpj = data.uni_cnpj
    let uni_cep = data.uni_cep
    let uni_logradouro = data.uni_logradouro
    let uni_complemento = data.uni_complemento
    let uni_numero = data.uni_numero
    let uni_bairro = data.uni_bairro
    let uni_cidade = data.uni_cidade
    let uni_estado = data.uni_estado
    let uni_pais = data.uni_pais
    let uni_email = data.uni_email
    let uni_created_by = data.uni_created_by

    let query = 'SELECT uni_unidade_id FROM uni_unidade WHERE uni_emp_empresa_id = ? AND uni_tipo = "M"'
    let values = [uni_emp_empresa_id]

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) return callback(error)
            if (results.length > 0 && uni_tipo == 'M') return callback({ error: 'Matriz já cadastrada' })
            query = 'INSERT INTO uni_unidade SET ?'
            values = [{ uni_razao_social: uni_razao_social, uni_nome_fantasia: uni_nome_fantasia, uni_emp_empresa_id: uni_emp_empresa_id, uni_tipo: uni_tipo, uni_cnpj: uni_cnpj, uni_cep: uni_cep, uni_logradouro: uni_logradouro, uni_complemento: uni_complemento, uni_numero: uni_numero, uni_bairro: uni_bairro, uni_cidade: uni_cidade, uni_estado: uni_estado, uni_pais: uni_pais, uni_email: uni_email, uni_created_by: uni_created_by }]
            pool.getConnection((err, connection) => {
                connection.query(query, values, (error, results, fields) => {
                    connection.release()
                    if (error) return callback(error)
                    return callback(null, { success: true })
                })
            })
        })
    })
}

function putId(param, data, callback) {
    // Atualizar informações da unidade por ID
    let query = 'SELECT uni.uni_unidade_id FROM uni_unidade uni JOIN uni_unidade emp ON uni.uni_emp_empresa_id = emp.uni_emp_empresa_id AND uni.uni_unidade_id != emp.uni_unidade_id AND emp.uni_tipo = "M" AND emp.uni_status = 1  WHERE uni.uni_unidade_id = ?'
    let values = [{ uni_unidade_id: param.id }]

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (err) return callback(err)
            if (results && results.length > 0 && (data.uni_tipo == 'M')) return callback({ error: 'Empresa já possui uma matriz.' })
            query = 'UPDATE uni_unidade SET ? WHERE ?'
            values = [{
                    uni_razao_social: data.uni_razao_social,
                    uni_nome_fantasia: data.uni_nome_fantasia,
                    uni_emp_empresa_id: data.uni_emp_empresa_id,
                    uni_pes_pessoa_id_contato: data.uni_pes_pessoa_id_contato,
                    uni_tipo: data.uni_tipo,
                    uni_cnpj: data.uni_cnpj,
                    uni_cep: data.uni_cep,
                    uni_logradouro: data.uni_logradouro,
                    uni_complemento: data.uni_complemento,
                    uni_numero: data.uni_numero,
                    uni_bairro: data.uni_bairro,
                    uni_cidade: data.uni_cidade,
                    uni_estado: data.uni_estado,
                    uni_pais: data.uni_pais,
                    uni_email: data.uni_email,
                    uni_status: data.uni_status
                },
                {
                    uni_unidade_id: data.uni_unidade_id
                }
            ]
            pool.getConnection((err, connection) => {
                connection.query(query, values, (error, results, fields) => {
                    connection.release()
                    if (error) return callback(error)
                    return callback(null, { success: true })
                })
            })

        })
    })
}

function deleteId(param, callback) {
    // Desativar a unidade
    let query = 'DELETE FROM uni_unidade WHERE ?'
    let values = [{ uni_unidade_id: param.id }]

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) return callback(error)
            return callback(null, { success: true })
        })
    })
}

module.exports = {
    get,
    getCompanies,
    getId,
    getCompanyUnities,
    post,
    putId,
    deleteId
}