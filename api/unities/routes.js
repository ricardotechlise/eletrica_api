const express = require("express")
const unitiesController = require("./unitiesController")
const verifyToken = require('../middlewares/verify')

function init() {
    let routes = express.Router()

    routes.route('/')
        .get(verifyToken, unitiesController.get)
        .post(verifyToken, unitiesController.post)

    routes.route('/companies')
        .get(unitiesController.getCompanies)

    routes.route('/companyunities/:id')
        .get(verifyToken, unitiesController.getCompanyUnities)

    routes.route('/:id')
        .get(verifyToken, unitiesController.getId)
        .put(verifyToken, unitiesController.putId)
        .delete(verifyToken, unitiesController.deleteId)

    return routes
}

module.exports = { init }